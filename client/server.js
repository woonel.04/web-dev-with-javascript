const port = 8889
const express = require("express")
const app = express();
const serveStatic = require("serve-static")
const errorHandling = require("./errorHandling/errorMessages")
const path = require("path")
const customerRoot = { root: "views/customer" }
const adminRoot = { root: "views/admin" }
const errorRoot = { root: "views/error" }

app.set("views", path.join(__dirname, "./views/customer"))
app.use(express.static("views"))
app.use("/static", express.static('./static/'));
app.set("view engine", "html")

app.use("/category", require("./controller/category.js"))
app.use("/login", require("./controller/login.js"))
app.use("/forgotpassword", require("./controller/forgotpassword.js"))
app.use("/products", require("./controller/products.js"))
app.use("/profile", require("./controller/profile.js"))
app.use("/register", require("./controller/register.js"))
app.use("/teapot", require("./controller/teapot.js"))
app.use("/shawn", require("./controller/shawn.js"))
app.use("/", require("./controller/index"))

app.use((req, res, next) => {
    res.sendFile("404.html", errorRoot)
})

app.listen(port, () => {
    console.log(`PORT RUNNING ON : ${port}`)
})