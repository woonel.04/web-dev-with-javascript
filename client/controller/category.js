const express = require("express");
const app = express();
const router = express.Router()
const bodyParser=require('body-parser');
const urlencodedParser=bodyParser.urlencoded({extended:false});

const customerRoot = {root: "views/customer"}
const userRoot = {root: "views/user"}
const adminRoot = {root: "views/admin"}


app.use(bodyParser.json()); //parse appilcation/json data
app.use(urlencodedParser);
app.set()

router.route("/")
.get(
    (req, res) => {
        res.sendFile("category.html", userRoot)
        // res.sendFile("")
    })

router.route("/post")
.get(
    (req, res)=> {
        res.sendFile("categoryPost.html", adminRoot)
    }
)

module.exports = router;