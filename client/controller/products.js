const express = require("express");
const app = express();
const router = express.Router()
const path = require("path")
const bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: false });

app.use(bodyParser.json()); //parse appilcation/json data
app.use(urlencodedParser);
const customerRoot = { root: "views/customer" }
const errorRoot = { root: "views/error" }
const userRoot = { root: "views/user" }
const adminRoot = { root: "views/admin" }

router.route("/post")
    .get(
        (req, res, next) => {
            res.sendFile("productsPost.html", adminRoot)
                // next()
        }
    )

router.route("/search")
    .get(
        (req, res, next) => {
            res.sendFile("404.html", errorRoot)
        })

router.route("/:productid")
    .get(
        (req, res, next) => {
            res.sendFile("productsGetOne.html", userRoot)
        })

router.route("/:productid/discount")
    .get(
        (req, res) => {
            res.sendFile("discountGetOne.html", adminRoot)
        }
    )
router.route("/")
    .get(
        (req, res, ) => {
            res.sendFile("productsGetAll.html", userRoot)
        })

module.exports = router;