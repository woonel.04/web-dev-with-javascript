const express = require("express");
const app = express();
const router = express.Router()
const bodyParser = require('body-parser');
const urlencodedParser = bodyParser.urlencoded({ extended: false });



const customerRoot = { root: "views/customer" }
const userRoot = { root: "views/user" }
const adminRoot = { root: "views/admin" }

app.use(bodyParser.json()); //parse appilcation/json data
app.use(urlencodedParser);
app.set()




//for the customer
router.route("/")
    .get(
        (req, res) => {
            res.sendFile("profile.html", customerRoot)
                // res.sendFile("profile")
        })


router.route("/edit")
    .get(
        (req, res) => {
            res.sendFile("profileEdit.html", customerRoot)
                // res.sendFile("profile")
        }
    )


router.route("/changePassword")
    .get(
        (req, res) => {
            res.sendFile("changePassword.html", customerRoot)
                // res.sendFile("profile")
        }
    )


//for the admins
router.route("/getAll")
    .get(
        (req, res) => {
            res.sendFile("profileGetAll.html", adminRoot)
        }
    )
    // router.route(")
module.exports = router;