commonSQLErrors = {
    1048: 'ER_BAD_NULL_ERROR',
    1049: 'ER_BAD_DB_ERROR',
    1054: 'ER_BAD_FIELD_ERROR',
    1062: 'ER_DUP_ENTRY',
    1406: 'ER_DATA_TOO_LONG',
}

statusCodeMessages = {
    200 : "Success", 
    201 : "Created",
    400 : "Bad Request",
    401 : "Unauthorized",
    403 : "Forbidden",
    404 : "Not Found",
    406 : "Not Acceptable",
    413 : "Payload Too Large",
    415 : "Unsupported Media Type",
    422 : "Unprocessable Entity",
    500 : "Internal Server Error",
}
module.exports = {
    genericError : 500,
    errorMessage : (statusCode) => {
        return {
            "status": statusCode,
            "message": statusCodeMessages[statusCode],
        }
    },
    sqlErrorStatus: (sqlErrNo)=> {
        if (sqlErrNo === 1049) {
            return 442
        }
        else {
            return 500
        }
    },
    sqlErrorMessages: (sqlErrNo) => {
        return {
            "status": 500,
            "message": `SQL ERROR: ${commonSQLErrors[sqlErrNo]}`,
        }
    },
}