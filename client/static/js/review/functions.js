function showReview(data) {
    console.log(data.length)
    if (data.length === 0) { reviewHTML = `<div class='mx-auto'><h6 class='text-muted '>No reviews yet</h6></div>` } else {
        reviewHTML = ``
        for (var review = 0; review < data.length; review++) {

            reviewHTML += `
        <div class="col-12 col-sm-6 col-4-md card mb-3" style="max-width: 540px;">
            <div class="row no-gutters">
                <div class="col-4" ><img src="${userUploadsUrl}/${data[review].profile_pic}" class="card-img" alt="profile_pic" id='profile_pic' style='height:50%;object-fit:contain;border-radius:100%;'></div>
                <div class="col-8">
                    <div class="card-body">
                        <h5 class="card-title">${data[review].username}</h5>
                        <div class='card-text' style='width:100%;'>
                        `

            for (star = 1; star <= 5; star++) {
                reviewHTML += `
                            <i class='fa fa-star${(star <= data[review].rating) ? '' : '-o'}'></i>
                            `
            }
            reviewHTML += `
                        </div>
                        <p class="card-text">${data[review].review}</p>
                        <p class="card-text"><small class="text-muted">${new Date(data[review].created_at).toLocaleString('en-uk', {year:"numeric", month:"short", day:"numeric", hour: '2-digit', minute: '2-digit' })}</small></p>
                    </div>
                </div>
            </div>
        </div>
      `
        }
    }
    $("#reviews").html(reviewHTML)
}


/////////////// ONLY FOR REGISTERED MEMBERS///////////////////////////
function showPostReviewBox() {
    postReviewHTML = `
    <legend style='text-align: center;' >Post Review</legend>
    <div id='postReviewResponse'></div>

<div class="form-group col-12 col-offset-3" style='display:flex; justify-content:center;' >
    <div class="stars " >
    <input class="star" id="5" type="radio" value='5' name="star" />
    <label class="star" for="5"></label>
    <input class="star" id="4" type="radio" value='4' name="star" />
    <label class="star" for="4"></label>
    <input class="star" id="3" type="radio" value='3' name="star" />
    <label class="star" for="3"></label>
    <input class="star" id="2" type="radio" value='2' name="star" />
    <label class="star" for="2"></label>
    <input class="star" id="1" type="radio" value='1' name="star" />
    <label class="star" for="1"></label>
</div>
</div>


<div class="form-group col-12 col-md-6 mx-auto" >
    <textarea  class="form-control" id="review"  placeholder="Give us your opinion" rows='3' maxlength='256' ></textarea>
  </div>
  <div class='col-12' >
    <div class='form-group col-12 col-md-6 mx-auto'>

  <button class='btn btn-success' onclick="postReview(${productid})">Post New Review</button>
  </div>
  </div>

  `
    $("#postReview").html(postReviewHTML)
}

function postReview() {
    rating = $("input[type=radio][name=star]:checked").val()
    review = $("#review").val()
    if (rating == null) {
        formErrorResponse('#postReviewResponse', 'Please rate this product!');
    }
    postReviewDetails = {
            rating: rating,
            review: review,
        }
        // console.log(postReviewDetails)
    axiosPostReview(postReviewDetails);
}