///////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// ON LOAD /////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////

function axiosGetOneProductReviews() {
    axios.get(`${backEndUrl}/review/${productid}`)
        .then((res) => { return showReview(res.data) }, () => { window.location.assign(frontEndUrl + "/products") })
}


// axios.create({
//     baseURL: backEndUrl
// })
// .get(`/review/${productid}`)
// .then((res) => {
//     console.log(res.data)
//     showReview(res.data)
// })
// .catch((error) => {
//     console.log(error)
//     console.log(error.response)
//     formErrorResponse("",error.response)
//         // $("#products").html(`<h1>${error.response.status}</h1><div>${error.response.statusText}</div>`)
//     window.location.assign(frontEndUrl + "/products")
// })


///////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// ON CLICK /////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////

function axiosPostReview() {

    axios
        .post(`${backEndUrl}/review/${productid}`, postReviewDetails)
        .then(() => {
            // formSuccessResponse('#postReviewResponse', 'Sucessfully Posted New Review!');
            window.location.reload();
        }, (error) => {
            if (error.response.status === 409) {
                formErrorResponse('#postReviewResponse', 'You have already reviewed this product!');

            } else {

                formErrorResponse('#postReviewResponse', 'Please rate this product!');
            }
        })
        .catch((error) => {
            formErrorResponse('#postReviewResponse', 'Unknown Server Error');
            // console.log(error.response)
        })
}