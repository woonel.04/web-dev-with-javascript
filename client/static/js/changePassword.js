function changePassword() {
    chkpwd();
    if (!chkpwd()) { return false; }

    axiosChangePassword = axios({
            method: 'put',
            url: `${backEndUrl}/profile/changePassword`,
            data: {
                oldpassword: $("#oldpassword").val(),
                newpassword: $("#password").val(),
            }
        })
        .then((res) => {
            formSuccessResponse("#responseField", "Password Successfully Changed!")
        }, (error) => {
            console.log(error.response)
            formErrorResponse("#responseField", error.response.data.message)
            $("#forgotPasswordHref").html(`<a href='/forgotpassword'>Forgot your password?</a>`)
        })

}