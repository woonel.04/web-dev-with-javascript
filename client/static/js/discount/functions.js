function showAllCategories(res) {



    discountHTML = `<h1 class='display-3 mx-auto' >Hot Sale!</h1>`
    for (var i = 0; i < res.data.length; i++) {
        discountHTML += `
        <div class='col-12 ' style='height:500px;'>
<div class="card " style='height:90%;'>
    <div class='discountCard' style='background-image:url("${productUploadsUrl}/${res.data[i].image_reference}");background-size:cover;height:100%;'>
        <div class="card-body" style='margin-top:10%;height:50%;background-color:rgba(0, 0, 0, 0.6);'>
            <h5 class="card-title" style='color:white;'>${res.data[i].name}</h5>
            <h6  style='color:red;font-size:1.7em;'>${res.data[i].discountname}</h6>
            <p class="card-text" style='color:white;'>Discount: ${res.data[i].discount}</p>
            <p class="card-text" style='color:white;'>From: ${new Date(res.data[i].start_date).toLocaleDateString('en-uk', {year:"numeric", month:"short", day:"numeric"})}</p>
            <p class="card-text" style='color:white;'>To: ${new Date(res.data[i].end_date).toLocaleDateString('en-uk', {year:"numeric", month:"short", day:"numeric"})}</p>    
            <a class='stretched-link' href='${frontEndUrl}/products/${res.data[i].productid}'></a>            
        </div>
    </div>
</div>
</div>
  `

    }
    $("#discounts").html(discountHTML)
}


function showDiscountsByProductId(res) {
    discountHTML = ''
    for (var i = 0; i < res.data.length; i++) {
        discountHTML += `
    <div class="card col-12 col-sm-6 col-md-3"  >
        <div class='discountCard' style='background-image:url("${productUploadsUrl}/${res.data[i].image_reference}");background-size:cover;height:300px;'>
            <div class="card-header bg-transparent border-bottom-0">
                <button  style='background-color:rgba(0, 0, 0, 0.6);padding:2px;border-radius:50px;'  type="button" class="close" aria-label="Close" onclick="axiosDeleteDiscount(${res.data[i].discountid})"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="card-body" style='margin-top:10%;height:fit-content;background-color:rgba(0, 0, 0, 0.6);'>
                <h5 class="card-title" style='color:white;'>${res.data[i].discountname}</h5>
                <p class="card-text" style='color:white;'>Discount: ${res.data[i].discount}</p>
                <p class="card-text" style='color:white;'>From: ${res.data[i].start_date.split("T")[0]}</p>
                <p class="card-text" style='color:white;'>To: ${res.data[i].end_date.split("T")[0]}</p>                
            </div>
        </div>
    </div>
      `
    }
    // <h6 class="card-subtitle text-muted">${res.data[i].discountname}</h6>
    $("#discounts").html(discountHTML)

}


///////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////// GET ONE ///////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////
function showAllDiscounts(res) {
    // console.log(res.data)
    discountHTML = '<h3>Discounts</h3>'

    discountHTML += `
        <div class='col-12' style='height:500px;'>
    <div class="card" style='height:100%;'>
        <div class='discountCard' style='background-image:url("${productUploadsUrl}/${res.data.image_reference}");background-size:cover;height:100%;'>
            <div class="card-body" style='margin-top:10%;height:60%;background-color:rgba(0, 0, 0, 0.6);'>
                <h5 class="card-title" style='color:red;font-size:1.7em'>${res.data.discountname}</h5>
                <p class="card-text" style='color:white;'>Discount: ${res.data.discount}%</p>
                <p class="card-text" style='color:white;'>${new Date(res.data.start_date).toLocaleDateString('en-uk', {year:"numeric", month:"short", day:"numeric"})} to  ${new Date(res.data.end_date).toLocaleDateString('en-uk', {year:"numeric", month:"short", day:"numeric"})}</p>                
            </div>
        </div>
    </div>
    </div>
      `
        // <h6 class="card-subtitle text-muted">${res.data[i].discountname}</h6>
    $("#discounts").html(discountHTML)

}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// POST DISCOUNT /////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// blockedDateRanges.push({ start_date: res.data[i].start_date, end_date: res.data[i].end_date })

function configForDateRange(res) {
    blockedDateRanges = [];
    for (i = 0; i < res.data.length; i++) {
        blockedDateRanges = appendBlockedDateRanges(res.data[i], blockedDateRanges)
    }

    console.log(blockedDateRanges)
    let picker = new Litepicker({
        element: document.getElementById('datepicker'),
        singleMode: false,
        allowRepick: true,
        lockDays: getDateArray(blockedDateRanges)
    });
    picker.render();
}



function appendBlockedDateRanges(details, arr) {
    arr.push({
        start_date: details.start_date,
        end_date: details.end_date,
    })
    return arr
}


function postDiscount() {
    var discountname = $("#discountname").val();

    var discount = $("#discount").val();
    //date validation
    var datepickerValDateRanges = [{
        start_date: $("#datepicker").val().split(" - ")[0],
        end_date: $("#datepicker").val().split(" - ")[1],
    }];
    arr1 = getDateArray(datepickerValDateRanges)
    arr2 = getDateArray(blockedDateRanges)

    const isBlocked = arr1.some(r => arr2.includes(r))
        // alert(isBlocked)


    //form validation
    if (discountname.length === 0 || discount.length === 0) {
        formErrorResponse("#postDiscountResponse", "Null values in form")
        return false

    }
    if (isBlocked) {
        formErrorResponse("#postDiscountResponse", "Please pick dates that are not blocked")
        return false
    }
    if (discount < 0 || discount > 100) {
        formErrorResponse("#postDiscountResponse", "Please enter a valid discount")
        return false
    }

    postDiscountDetails = {
        discountname: discountname,
        discount: discount,
        start_date: $("#datepicker").val().split(" - ")[0],
        end_date: $("#datepicker").val().split(" - ")[1],
    }
    axiosPostDiscount(postDiscountDetails);


    //
}


function getDateArray(dateRanges) {
    arr = [];
    for (var dateRange = 0; dateRange < dateRanges.length; dateRange++) {
        start = dateRanges[dateRange].start_date;
        end = dateRanges[dateRange].end_date;
        var dtStart = new Date(start);
        var dtEnd = new Date(end);
        while (dtStart <= dtEnd) {
            arr.push(dtStart.toISOString().split('T')[0])
            dtStart.setDate(dtStart.getDate() + 1);
        }
    }
    return arr


}