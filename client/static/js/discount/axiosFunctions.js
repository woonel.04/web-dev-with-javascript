///////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// GET ALL /////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////

///////////// FOR INDEX.HTML//////////
function axiosGetAllDiscounts() {
    axios
        .get(`${backEndUrl}/discount`)
        .then(res => { if (res.data.length > 0) showAllCategories(res); });
}




///////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// GET FILTERED DISCOUNT ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////
//Filtered By Date

function axiosGetFilteredDiscountByProductId() {
    axios.get(`${backEndUrl}/discount/${productid}/filtered`)
        .then((res) => {
            if (res.data) {
                showAllDiscounts(res)
                $("#price").css({ 'text-decoration': 'line-through' })
                $("#discounted_price").html(parseFloat(res.data.discounted_price).toFixed(2))
            }
        })

}

///////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// POST DISCOUNT /////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////


function axiosGetAllDiscountsAndPostDiscount() {
    axios.get(`${backEndUrl}/discount/${productid}`)
        .then((res) => {
            showDiscountsByProductId(res);
            configForDateRange(res);
        }, () => {})
}


function axiosPostDiscount(postDiscountDetails) {

    axios
        .post(`${backEndUrl}/discount/${productid}`, postDiscountDetails)
        .then(() => {
            // formSuccessResponse("#postDiscountResponse", "Successfully created new discount")
            window.location.reload();
        }, (error) => {
            console.log(error.response)
            formErrorResponse("#postDiscountResponse", error.response.message)
        })
        .catch((error) => {

            console.log(error.response)
            formErrorResponse("#postDiscountResponse", error.response.message)
        })
}




/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// DELETE DISCOUNT ///////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function axiosDeleteDiscount(discountid) {
    // alert(discountid)
    axios
        .delete(`${backEndUrl}/discount`, {
            data: {
                discountid: discountid,
            },
        })
        .then(() => {
            window.location.reload();
        }, () => {
            alert("Unknown Back End Error")
        })

}