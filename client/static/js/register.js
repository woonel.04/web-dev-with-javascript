function postUser() {


    // passwordField = document.getElementById("passwordField").innerHTML
    // contactField = document.getElementById("contactField").innerHTML
    // if (passwordField.length > 0 || contactField.length > 0) {
    //     return false
    // }

    username = $("#username").val();
    email = $("#email").val();
    contact = $("#contact").val();
    password = $("#password").val();

    if (username.length === 0 || email.length === 0 || contact.length === 0 || password.length === 0) {
        formErrorResponse("#formResponse", "Please fill up the form")
        return false
            // alert('help')
    }

    emailResult = chkemail();
    pwdResult = chkpwd();
    contactResult = chkcontact();

    if (!(emailResult && pwdResult && contactResult)) { return false; }


    newUser = {
        username: $("#username").val(),
        email: $("#email").val(),
        contact: $("#contact").val(),
        password: $("#password").val(),
    }
    axios
        .post(backEndUrl + "/register", newUser)
        .then((res) => {
            console.log(res.data)
            window.location.assign(frontEndUrl + "/login")
        })
        .catch(err => {
            // console.log(err.response.message)
            chkusername(err.response.data.message)
                // formErrorResponse("#formResponse", err.response.data.message)
        });

}