loginUser = () => {
    const user = {
        username: $("#username").val(),
        password: $("#password").val()
    }
    axios
        .post(backEndUrl + "/login", user)
        .then((res) => {
            localStorage.setItem('token', res.data.token);
            localStorage.setItem('userInfo', JSON.stringify(res.data.userInfo));
            window.location.assign(frontEndUrl)
        })
        .catch((err) => {
            console.log(err.response)
            if (err.response.data.message == "Password is Incorrect") {
                console.log(err.response.data.message)
                $("#forgotPasswordHref").html("<a href='/forgotpassword'>Forgot your password?</a>")
                    // document.getElementById("forgotPasswordButton").innerHTMl = "<a href='/login/forgot'>Forgot your password?</a>"
            }
            noLoginUser(err.response)
        })
}
noLoginUser = (error) => {
    $("#errorMessage").html(error.data.message)
    $("#errorMessage").css({
            color: "red"
        })
        // document.getElementById('errorMessage').innerHTML = error.data.message


}


//   document.getElementById('get').addEventListener('click', getTodos);
document.getElementById('login').addEventListener('click', loginUser);