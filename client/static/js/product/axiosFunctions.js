//////////////////////////////////////////////////
/////////////////// GET ALL /////////////////////
////////////////////////////////////////////////


/////////////////// ON LOAD ///////////////////////////

async function axiosGetAllProducts() {
    await axios
        .get(`${backEndUrl}/products`)
        .then(res => { return showAllProducts(res) }, () => { errorResponse("#products", "Unknown Error") })
        .catch(() => { errorResponse("#products", "Unknown Error") })

    await axios
        .get(`${backEndUrl}/discount`)
        .then((res) => {
            res.data.map(discountedDetails => {
                $(`#price-${discountedDetails.productid}`).css({ "text-decoration": "line-through" })
                $(`#discount-${discountedDetails.productid}`).html(parseFloat(discountedDetails.discounted_price).toFixed(2))
            })
        })
}


////////////////////////////////////////////////////////////
/////////////// FOR THE SEARCH FUNCTION ////////////////////
////////////////////////////////////////////////////////////

function axiosGetHighest() {
    // this function is to set the slider values
    axios
        .get(`${backEndUrl}/products/getHighest`)
        .then(res => { showSlider(parseInt(res.data[0]["max(price)"]) + 1) })
        .catch((error) => { formErrorResponse("#errorField", "Unknown Error") })
};

function axiosGetCategoriesCheckboxes() {
    axios
        .get(`${backEndUrl}/category`)
        .then(res => { showCategoryCheckbox(res) }, () => { errorResponse("#categories", 'NO categories so far!') })
        .catch((error) => { errorResponse("#categories", 'NO categories so far!') })

}


/////////////////// ON CLICK ///////////////////////////


function axiosSearch(queryString) {

    axios
        .get(`${backEndUrl}/products/search`, {
            params: queryString
        })
        .then(res => {
            showAllProducts(res)
        })
        .catch(err => {
            errorResponse("#products", 'No products like that')

        });
}




//////////////////////////////////////////////////
/////////////////// GET ONE /////////////////////
////////////////////////////////////////////////


//////////////////// ON LOAD ///////////////////////
function axiosGetOneProduct() {
    axios.get(`${backEndUrl}/products/${productid}`)
        .then((res) => { return showProduct(res) }, (error) => { window.location.assign(frontEndUrl + "/products") })

    axios.get(`${backEndUrl}/products/${productid}/productImage`)
        .then((res) => { return imageCarousel(res) }, () => { window.location.assign(frontEndUrl + "/products") })
    axiosGetOneProductReviews();

}
/////////////////// ON CLICK ////////////////////////
function axiosDeleteProduct(productid) {
    axios
        .delete(`${backEndUrl}/products/${productid}`)
        .then(() => {
            window.location.reload("/products")
        }, (error) => {
            console.log(error.response)
            alert(JSON.stringify(error.response.message))
        })
}









/////////////////////////////////////////////////////////////////////////////////////////
///////////////////////// PRODUCTS POST ////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////


function axiosGetCategoriesSelect() {
    axios
        .get(`${backEndUrl}/category`)
        .then(res => {
            showCategorySelect(res)

        }, () => { errorResponse("#category", 'NO categories so far!') })
        .catch((error) => {
            errorResponse("#category", 'NO categories so far!')
        })

}


function axiosPostProduct(data) {
    axios
        .post(`${backEndUrl}/products/post`, data)
        .then((res) => {
            formSuccessResponse("#res", "Created")
            createFormToPostProductImage(res.data.productid)
        })
        .catch((err) => {
            formErrorResponse("#res", "This Product Already Exists!")
        })

}

function postProductImage(productid) {

    var bodyFormData = new FormData();

    bodyFormData.append('image', image.files[0]);
    axios
        ({
            method: 'POST',
            url: `${backEndUrl}/products/${productid}/productImage`,
            data: bodyFormData,
            headers: { "Content-Type": "multipart/form-data" },
        })
        .then((res) => {
            console.log(res)
                // showOutput(res)
            formSuccessResponse("#res", "Created")
            createFormToPostProductImage(res.data.productid)


        })
        .catch((err) => {
            console.log("this is the res data")
            console.log(err.response.data)
                // res.status(500)
                // showOutput(err.response.data);
            formErrorResponse("#res", "Generic Error")
        })
}