//////////////////////////////////////////////////
/////////////////// GET ALL /////////////////////
////////////////////////////////////////////////


showAllProducts = (res) => {
    content = ''
    for (var i = 0; i < res.data.length; i++) {
        content += `
        <div class='col-12 col-sm-6 col-md-3 my-3'>
        <div class="card " >
        <img src="${productUploadsUrl}/${res.data[i].image_reference}" class="card-img-top" alt="${res.data[i].name}">
        
        <div class="card-body">
          <h5 class="card-title">${res.data[i].name}</h5>
          <p class="card-text">${res.data[i].brand}</p>
          <p class="card-text text-muted" >Rating: ${(res.data[i].avg_rating) ? parseFloat(res.data[i].avg_rating).toFixed(1) : 'No Ratings Yet'}</p>
          <p class="card-text" style='color:red'>$<span id='price-${res.data[i].productid}'>${res.data[i].price}</span><span id='discount-${res.data[i].productid}' style='margin-left:10px;font-size:1.3em'></span></p>
          <p class="card-text">category: ${res.data[i].category}</p>
          <a href="${frontEndUrl}/products/${res.data[i].productid}" class="stretched-link"></a>
        </div>
      </div>
      </div>
      `
    }
    $("#products").html(content)
}


function showCategoryCheckbox(res) {

    HTML = ''
    for (var i = 0; i < res.data.length; i++) {
        HTML += `<input  class='ml-3 mr-1' name='categories' type="checkbox" id="${res.data[i].categoryid}" value="${res.data[i].category}"/>${res.data[i].category}`
    }
    $("#categories").html(HTML)

}


function showSlider(upperBound) {

    $("#slider-range").slider({
        range: true,
        min: 0,
        max: upperBound,
        values: [0, upperBound],
        slide: function(event, ui) {
            $("#amount").val("$" + ui.values[0] + " - $" + ui.values[1]);
        }
    });
    $("#amount").val("$" + $("#slider-range").slider("values", 0) +
        " - $" + $("#slider-range").slider("values", 1));

}

function searchProduct() {
    console.log($("#slider-range"))
    var checkboxArray = []
    var checkboxes = document.querySelectorAll('input[type=checkbox]:checked')

    for (var i = 0; i < checkboxes.length; i++) {
        checkboxArray.push(checkboxes[i].value)
    }

    priceRange = (event, ui) => {
        // console.log(ui.values[0])
        return {
            lowerPrice: ui.values[0],
            higherPrice: ui.values[1],
        }
    };
    // alert($("#slider-range").slider("values", 0))
    axiosSearch({
        name: $("#name").val(),
        lowerPrice: $("#slider-range").slider("values", 0),
        higherPrice: $("#slider-range").slider("values", 1),
        category: checkboxArray.toString()
    })
}







//////////////////////////////////////////////////
/////////////////// GET ONE /////////////////////
////////////////////////////////////////////////


function showProduct(res) {
    // console.log(res.data)
    $("title").html(res.data.name)
    $("#main-image").attr("src", productUploadsUrl + "/" + res.data.image_reference)
    $("#name").text(res.data.name)
    $("#price").html(res.data.price)
    $("#description").text(res.data.description)
    $("#brand").text(res.data.brand)
    $("#rating").attr("href", "/products/" + res.data.productid + "/#reviews")
    $("#avg_rating").text((res.data.avg_rating) ? parseFloat(res.data.avg_rating).toFixed(1) : 'No ratings')
}

function imageCarousel(res) {
    // console.log(`${productUploadsUrl}/${res.data[i].image_reference}`)
    console.log(res.data)
    content = ''
    for (var i = 0; i < res.data.length; i++) {
        content += ` <img onclick="change_image(this)" src="${productUploadsUrl}/${res.data[i].image_reference}" width="70"> `
    }
    $("#carouselOfImages").html(content)
}


function change_image(image) {
    var container = document.getElementById("main-image");
    container.src = image.src;
}




///////////////////// ADMINS ///////////////////////////


function showDeleteProductButton() {
    deleteHTML = `
    <button class='btn btn-info float-right' onclick='axiosDeleteProduct(${productid})'>Delete Product</button>`
    $("#deleteProduct").html(deleteHTML)

}

/////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////// POST PRODUCT///////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
function showCategorySelect(res) {

    HTML = ''
    for (var i = 0; i < res.data.length; i++) {
        HTML += `
    <option value="${i + 1}">${res.data[i].category}</option>
    `
    }
    $("#category").html(HTML)
}


function createNewProduct() {
    var name = $("#name").val();
    var description = $.trim($("#description").val());
    var categoryid = $("#category").val();
    var brand = $("#brand").val();
    var price = $("#price").val()

    if (name == null || description == null || categoryid == null || brand == null || price == null) {
        alert("Null values in form")
        return false;
    }
    if (price.indexOf("e") != -1) {
        alert("Invalid Price")
        return false;
    }
    if (!(price.length - price.indexOf(".") == 3 || price.length - price.indexOf(".") == 2 || price.indexOf(".") == -1)) {
        alert("Price must only have 2 decimal places ")
        return false;

    }
    data = {
        name: name,
        description: description,
        categoryid: categoryid,
        brand: brand,
        price: price,
    }
    axiosPostProduct(data)

}

function createFormToPostProductImage(productid) {

    HTML = `
    <a class='btn btn-info' href='${frontEndUrl}/products/${productid}'>Go To Product</a>
    <br>
        <input type='file' id='image'>
        <button type='submit' class="btn btn-info" id="post" onclick='postProductImage(${productid})'>POST IMAGE</button>
        <div id='postImageError'></div>
        `

    $("#postImagesForm").html(HTML)
}