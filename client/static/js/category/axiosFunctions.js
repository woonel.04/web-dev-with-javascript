///////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// ON LOAD /////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////

function axiosGetAllCategories() {
    axios.create({
            // Other custom settings
            baseURL: backEndUrl
        })
        .get('/category')
        .then(res => {
            console.log(res.data)
            showAllCategories(res);
            if (token && userInfo.role === "Admin") showCategoryPost();
        });
}



///////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// ON CLICK /////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////


function axiosPostCategory(data) {
    return axios
        .post(`${backEndUrl}/category`, data)
        .then((res) => {
            console.log(res)
            return window.location.reload();


        })
        .catch((err) => {
            console.log("this is the res data")
            console.log(err.response.data)
            return formErrorResponse("#responseField", err.response.data.message)
                // res.status(500)
                // showOutput(err.response);
        })


}