function showAllCategories(res) {
    categoryHTML = ''
    for (var i = 0; i < res.data.length; i++) {
        categoryHTML += `
        <div class='col-12 col-sm-6 col-md-3'>
        <div class="card m-3" >
        
        <div class="card-body ">
          <h5 class="card-title">${res.data[i].category}</h5>
          <p class="card-text">${res.data[i].description}</p>
        </div>
      </div>
      </div>
      `
    }
    $("#category").html(categoryHTML)
}



function showCategoryPost() {
    categoryHTML = `
  <div class="card col-12 col-sm-6 col-md-3" >


    
    <div class="card-body">
    <h5 class='card-title pull-left'>New Category<button class='btn-sm btn-info pull-right mr-0 ml-3' onclick='createNewCategory()'>Create New</button></h5>


<div class="md-form ">
<label for="categoryname">Category Name</ label>
  <textarea id="categoryname" class="md-textarea form-control" rows="2"></textarea>
</div>

<div class="md-form">
<label for="description">Description</label>
  <textarea id="description" class="md-textarea form-control" rows="2" maxlength='45'></textarea>
</div>

    </div>
    </div>

    `

    $("#category").append(categoryHTML)

}

function createNewCategory() {
    category = $("#categoryname").val();
    description = $("#description").val();
    if (category.length === 0 || description.length === 0) {
        // showOutput({ status: 400, data: { message: 'Null Value In Form' } })
        formErrorResponse("#responseField", "Null Value In Form")
            // formErrorResponse("#formResponse", "Null Values In Form")

        return false;

    }
    newCategory = {
        category: category,
        description: description,
    }
    axiosPostCategory(newCategory);
}


function showNewCategoryCard(details) {
    document.getElementById('category').innerHTML += `


    <div class="card col-12 col-sm-6 col-md-3" >
        
    <div class="card-body">
      <h5 class="card-title">Status: ${details.status}</h5>
      <p class="card-text">${JSON.stringify(details.data.message, null, 2)}</p>
    </div>
  </div>
          `;
}