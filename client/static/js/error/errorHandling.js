function errorResponse(boxId, customMessage) {
    HTML = `<div class="p-3 mb-2 bg-secondary text-white" style='width:100%;'><span style='padding-left:5%;'>${customMessage}</span></div>`
    $(boxId).html(HTML)

}

function formSuccessResponse(boxId, customMessage) {


    HTML = `
        <div class="alert alert-success alert-dismissible fade show">
            <p>${customMessage}</p>
        </div>
        `
    $(boxId).html(HTML)

    $(".alert").fadeTo(2000, 500).slideUp(500, function() {
        $(".alert").slideUp(500);
    });
}



function formErrorResponse(boxId, customMessage) {


    HTML = `
        <div class="alert alert-danger alert-dismissible fade show">
            <p>${customMessage}</p>
        </div>
        `
    $(boxId).html(HTML)



    $(".alert").fadeTo(2000, 500).slideUp(500, function() {
        $(".alert").slideUp(500);
    });
}