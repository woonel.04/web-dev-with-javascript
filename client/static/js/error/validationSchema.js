function chkcontact() {
    var contact = $("#contact").val();
    if (contact < 80000000 || contact > 99999999 || contact.search('e') > -1) {
        errorMessage = "Please enter a valid SG phone number"
    } else { errorMessage = '' }

    $("#contactField").html(errorMessage)
    $("#contactField").css({ color: 'red' })
    if (errorMessage.length > 0) { return false; } else { return true; }


}








// Show output in browser


function chkpwd() {
    var password = $("#password").val();
    if (password.length < 8) {
        errorMessage = "Password Length Must be At Least 8 Characters"
    } else if (password.search(/[0-9]/) === -1) {
        errorMessage = "Password must have at least 1 number"
    } else if (password.search(/[a-z]/) === -1 && password.search(/[A-Z]/) === -1) {
        errorMessage = "Password must have at least 1 letter"
    } else if (password.search(/[!/@/#/$/%/^/&/*/(/)/_/+/./,/;/:]/) === -1) {
        errorMessage = "Password must have at least 1 unique character"
    } else { errorMessage = '' }

    $("#passwordField").html(errorMessage)
    $("#passwordField").css({ color: 'red' })
    if (errorMessage.length > 0) { return false; } else { return true; }
}


function chkemail() {
    var email = $("#email").val();
    if (email.search(/[@/.]/) === -1) {
        errorMessage = "Please enter a valid email"
    } else if (email.search(/[@]/) >= (email.lastIndexOf('.') - 2)) {
        errorMessage = "Please enter a valid email"
    } else { errorMessage = '' }

    $("#emailField").html(errorMessage)
    $("#emailField").css({ color: 'red' })
    if (errorMessage.length > 0) { return false; } else { return true; }
}

function chkusername(errorMessage) {

    $("#usernameField").html(errorMessage)
    $("#usernameField").css({ color: 'red' })


}