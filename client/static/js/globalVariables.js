// AXIOS GLOBALS
const frontEndUrl = 'http://localhost:8888'
const backEndUrl = 'http://localhost:8000/api';
const userUploadsUrl = 'http://localhost:8000/uploads/userUploads'
const productUploadsUrl = 'http://localhost:8000/uploads/productUploads'
const imagesUrl = 'http://localhost:8000/uploads/images'
const faviconUrl = 'http://localhost:8000/uploads/images/favicon.ico'
const userInfo = JSON.parse(localStorage.getItem("userInfo"))
const token = localStorage.getItem("token");

addTokenToHeaders = () => {
    if (token) {
        axios.defaults.headers.common['Authorization'] = token;
    } else {
        axios.defaults.headers.common['Authorization'] = null;
    }
}
addTokenToHeaders();