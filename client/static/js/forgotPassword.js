forgotPassword = () => {
    const username = $("#username").val()

    axios
        .put(backEndUrl + '/login/forgot', {
            username: username
        })
        .then(res => {
            // console.log(res)
            formSuccessResponse("#errorMessage", `New Password sent to ${res.data.email}`)
                // document.getElementById("errorMessage").innerHTML = `New Password sent to ${res.data.email}`
        })

    .catch(err => {
        formErrorResponse("#errorMessage", err.response.data.message)
            // $("#errorMessage").html(err.response.data.message)
            // $("#errorMessage").css({
            //     color: 'red'
            // })
            // console.log(err.response.data.message)
    });
}


//   document.getElementById('get').addEventListener('click', getTodos);
document.getElementById('sendEmail').addEventListener('click', forgotPassword);