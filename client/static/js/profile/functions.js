function updateUserDetails(details) {
    $("#username").val(details.username)
    $("#email").val(details.email)
    $("#contact").val(details.contact)
    $("#image").val(details.image)
}

function getUserDetails(details) {
    $("title").html(details.username)
    $("#username").html(details.username)
    $("#email").html(details.email)
    $("#contact").html(details.contact)
    $("#role").html(details.role)
    $("#image").attr("src", userUploadsUrl + "/" + details.profile_pic)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////// UPDATE USER /////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////

function updateUser() {
    username = $("#username").val();
    email = $("#email").val();
    contact = $("#contact").val();
    console.log(username)
    if (username.length === 0 || email.length === 0 || contact.length === 0) {
        formErrorResponse("#errorField", "Null values in form")
        return false;
    }
    // alert(password)

    // if (contact.length > 0) {
    // }
    emailResult = chkemail();
    contactResult = chkcontact();

    if (!(emailResult && contactResult)) { return false; }


    data = {
        username: $("#username").val(),
        email: $("#email").val(),
        contact: $("#contact").val(),
    }
    axiosUpdateUser(data);
};



function changeUserProfileImage() {

    var bodyFormData = new FormData();
    if (image.files[0] == null) {
        formErrorResponse({ message: "No file uploaded" })
        return false;
    } else {

        bodyFormData.append('image', image.files[0]);
        axiosUpdateUserImage(bodyFormData)

    }
};


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// FOR ADMINS //////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function showAllProfiles(allUsers) {
    HTML = ``
    for (var i = 0; i < allUsers.length; i++) {
        HTML += `
    <div class="card col-3" >
      <img class="card-img-top" src="${userUploadsUrl}/${allUsers[i].profile_pic}" alt="User Image">
      <div class="card-body">
        <h4 class="card-title">Username: ${allUsers[i].username}</h4>
        <p class="card-text">
          <p id='email'>Email: ${allUsers[i].email}</p>
          <p id='contact'>Contact: ${allUsers[i].contact}</p>
          <p id='role'>Role: ${allUsers[i].role}</p>
        </p>
      </div>
    </div>
`
    }
    $("#allProfiles").html(HTML)

}


///////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////// INTERESTS /////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
function showInterests() {

    new Promise(async(resolve, reject) => {
            categoryData = await axiosGetCategory();

            interestData = await axiosGetInterestData();

            // return resolve(showInterests(categoryData, interestData))

            HTML = ''
            for (var categoryindex = 0; categoryindex < categoryData.length; categoryindex++) {
                HTML += `<input   type="checkbox" id="${categoryData[categoryindex].categoryid}" value="${categoryData[categoryindex].category}"`
                for (var interestindex = 0; interestindex < interestData.length; interestindex++) {
                    if (categoryData[categoryindex].categoryid === interestData[interestindex].category_id) { HTML += ` checked`; }
                }

                HTML += `
         />${categoryData[categoryindex].category}
        <br>
                    `
            }
            HTML += `<button class='btn btn-info' onclick=postInterests()>Submit New Preferences</button>`
            $("#interests").html(HTML)

        })
        .then(() => { return }, () => { errorResponse("#interests", "Error with interests") })
        .catch(() => { errorResponse("#interests", "Error with interests") })

}

function postInterests() {

    var checkboxArray = []
    var checkboxes = document.querySelectorAll('input[type=checkbox]:checked')

    for (var i = 0; i < checkboxes.length; i++) {
        checkboxArray.push(checkboxes[i].getAttribute('id'))
    }
    console.log(checkboxArray.toString())
    axiosPostInterests({ categoryids: checkboxArray.toString() })
}