///////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// ON LOAD /////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////// EDIT PROFILE ////////////////////////////
function axiosEditProfile() {
    axios.get(`${backEndUrl}/profile`)
        .then(res => { getUserDetails(res.data) });
}


///////////////////////////// GET PROFILE ///////////////////////////

function axiosGetYourProfile() {
    axios.get(`${backEndUrl}/profile`)
        .then(res => {
            getUserDetails(res.data)
        }, (error) => {
            console.log(`error is: ${error}`)
            if (error.response.data.status === 403 || error.response.data.status === 401) {
                clearStorage()
                window.location.assign(frontEndUrl + 'login')
            } else {
                window.location.assign("/profile")
            }
        });

}

/////////////////////////// EDIT PROFILE ///////////////////////////////////////////////////

function axiosEditYourProfile() {
    axios.get(`${backEndUrl}/profile`)
        .then(res => {
            updateUserDetails(res.data)
        }, (error) => {
            console.log(`error is: ${error}`)
            if (error.response.data.status === 403 || error.response.data.status === 401) {
                clearStorage()
                window.location.assign(frontEndUrl + 'login')
            } else {
                window.location.assign("/profile")
            }
        });

}

///////////////////////// GET ALL PROFILES (FOR ADMINS) /////////////////////////////////////////
function axiosGetAllProfiles() {
    axios.get(`${backEndUrl}/profile/getAll`)
        .then(res => { showAllProfiles(res.data) }, () => { window.location.assign("/profile") })
}


/////////////////////////////// INTEREST DATA //////////////////////////////////////////////////////////////////

function axiosGetCategory() {
    return axios
        .get(`${backEndUrl}/category`)
        .then(res => { return res.data })
        .catch((error) => { return })
}

function axiosGetInterestData() {
    return axios
        .get(`${backEndUrl}/profile/${userInfo.id}/interest`)
        .then(res => { return res.data })
        .catch((error) => { return })
}
///////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// ON CLICK /////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////


function axiosUpdateUser(data) {
    axios
        .put(`${backEndUrl}/profile`, data)
        .then(res => {
            console.log(res)

            formSuccessResponse("#errorField", res.data.message)
        })
        .catch(err => {
            formErrorResponse("#errorField", err.response.data.message)
        });
}


function axiosUpdateUserImage(data) {
    axios({
            method: "PUT",
            url: backEndUrl + "/profile/profileImage",
            data: data,
            headers: { "Content-Type": "multipart/form-data" },
        })
        .then(res => {
            console.log(res.data)
            formSuccessResponse("#errorField", res.data.message)
        })
        .catch(err => {
            console.log(err.response)
            formErrorResponse("#errorField", err.response.data.message)
        });

}


///////////////////// FOR ADMINS ////////////////////////////////////////////

function axiosSearchProfile() {
    data = {
        name: $("#name").val(),
        role: $("input[type=radio]:checked").val(),
    }
    axios

        .get(backEndUrl + '/profile/search', {
            params: data
        })
        .then(res => {
            // console.log(res.data)
            showAllProfiles(res.data)
        }, (err) => {
            errorResponse("#allProfiles", 'No users like that')

        })
        .catch(err => {
            errorResponse("#allProfiles", 'No users like that')
        });
}



/////////////////////////// POST INTERESTS ////////////////////////////////////////////////////////

function axiosPostInterests(data) {
    axios
        .post(`${backEndUrl}/profile/${userInfo.id}/interest`, data)
        .then(() => { formSuccessResponse("#successfulEdit", "New Preferences Updated!") }, () => { formErrorResponse("#successfulEdit", 'Error when changing preferences') })

}