// originalObject = {
//     aloyisus: 12345,
//     benedict: "no",
//     carlisle: undefined,
//     dobre: null,
//     esmeralda: 0,
//     fitzgerald: [null, null, null],
//     gerald : [0, 0, 0 , 0 ],
//     harold_styles: [123,"cocochanel", ["mik_tyson"]],
//     idris: [undefined, undefined, undefined],
//     june_wen: [undefined, "cocktail"],
//     koh: [],
// }
module.exports = (originalObject) => {
    newKeys = []
    newObject = {}


    for (key of Object.keys(originalObject)) {
        ////////////////////////// FOR ARRAYS ////////////////////////////////////
        if (originalObject[key] instanceof Array) {
            originalObject[key] = originalObject[key].filter((value) => value != undefined)
            if (originalObject[key].length > 0) { newKeys.push(key) }
        }
        ////////////////////////// OTHERS (strings, numbers, etc.) ////////////////////////////////////
        else {
            if (originalObject[key] != null && originalObject[key].length > 0) { newKeys.push(key) }
        }
    }

    for (var i = 0; i < newKeys.length; i++) {
        newObject[newKeys[i]] = originalObject[newKeys[i]]
    }
    return newObject
}