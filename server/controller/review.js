const express = require("express");
const app = express();
const router = express.Router()
const verification = require('../auth/verification.js');
const product = require("../model/product")
const errorHandling = require("../errorHandling/errorMessages")
const bodyParser = require('body-parser');
const urlencodedParser = bodyParser.urlencoded({ extended: false });
const cleaningUndefined = require("../errorHandling/cleaningUndefined")
app.use(bodyParser.json()); //parse appilcation/json data
app.use(urlencodedParser);






////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////// REVIEWS /////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



const review = require("../model/review")



router.route("/:productid")
    .get(
        (req, res) => {
            review.getReview(req.params)
                .then((result) => {
                    res.status(200)
                    res.json(result)
                }, (error) => {
                    res.status(errorHandling.sqlErrorStatus(error.errno))
                    console.log(errorHandling.sqlErrorMessages(error.errno))
                    res.json(errorHandling.sqlErrorMessages(error.errno));
                })
                .catch((err) => {
                    console.log(errorHandling.errorMessage(500))
                    res.status(500)
                    res.json(errorHandling.errorMessage(500))
                })
        }
    )
    .post(
        verification.verifyToken,
        urlencodedParser,
        (req, res) => {

            detailsPostReview = {
                productid: req.params.productid,
                userid: req.userid,
                rating: req.body.rating,
                review: req.body.review,
            }
            review.postReview(detailsPostReview)
                // .then((result)=> {}, ()=> {})
                .then((sendToClient) => {
                    res.status(201)
                    res.json({ success: true, productid: sendToClient })
                }, (error) => {
                    res.status(errorHandling.sqlErrorStatus(error.errno))
                    console.log(errorHandling.sqlErrorMessages(error.errno))
                    res.json(errorHandling.sqlErrorMessages(error.errno));
                })
                .catch((err) => {
                    console.log(errorHandling.errorMessage(500))
                    res.status(500)
                    res.json(errorHandling.errorMessage(500))
                })

        }
    )









module.exports = router;