const express = require("express");
const app = express();
const router = express.Router()
const verification = require('../auth/verification.js');
const profile = require("../model/profile")
const errorHandling = require("../errorHandling/errorMessages")
const bodyParser = require('body-parser')
require("dotenv").config('../.env')
var urlencodedParser = bodyParser.urlencoded({ extended: false });
const category = require("../model/category")
app.use(bodyParser.json()); //parse appilcation/json data
app.use(urlencodedParser);



// router.route("/:categoryid")
// .get(
//     (req, res)=> {
//         category.getProductsByCategoryid(req.params.categoryid)
//         .then((result)=> {
//             // console.log(result)
//             res.status(200).json(result)
//         }, (error)=>{            
//             res.status(errorHandling.sqlErrorStatus(error.errno))
//             res.json(errorHandling.sqlErrorMessages(error.errno))
//         },)
//         .catch((error)=> {
//             console.log("Unknown Server Error")
//             res.status(500)
//             res.send(errorHandling.errorMessage(500))
//         })
//     }
// )
router.route("/post")
    .get(
        verification.verifyAdminToken,
        (req, res) => {
            res.status(200).json({ "message": "OK" })
        }
    )

router.route("/")
    .get(
        (req, res) => {
            category.getCategories()
                .then((result) => {
                    // console.log(result)
                    res.status(200).json(result)
                }, (error) => {
                    res.status(errorHandling.sqlErrorStatus(error.errno))
                    res.json(errorHandling.sqlErrorMessages(error.errno))
                }, )
                .catch((error) => {
                    console.log("Unknown Server Error")
                    res.status(500)
                    res.send(errorHandling.errorMessage(500))
                })
        }
    )
    .post(
        urlencodedParser,
        verification.verifyAdminToken,
        async(req, res) => {
            category.postCategory(req.body)
                .then(() => {
                    res.status(201).json({ message: "Created" })
                }, (error) => {
                    if (error.errno === 1062 || error.code === 'ER_DUP_ENTRY') {
                        res.status(409).json({ message: "Category already exists" })
                    } else {
                        res.status(errorHandling.sqlErrorStatus(error.errno))
                        res.json(errorHandling.sqlErrorMessages(error.errno))
                    }
                })
                .catch((error) => {
                    console.log("Unknown Server Error")
                    res.status(500)
                    res.send(errorHandling.errorMessage(500))
                })
        })

module.exports = router;