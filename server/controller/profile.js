const express = require("express");
const app = express();
const router = express.Router()
var verification = require('../auth/verification.js');
const profile = require("../model/profile")
const errorHandling = require("../errorHandling/errorMessages")
const cleaningUndefined = require("../errorHandling/cleaningUndefined")

const bodyParser = require('body-parser');
const urlencodedParser = bodyParser.urlencoded({ extended: false });

app.use(bodyParser.json()); //parse appilcation/json data
app.use(urlencodedParser);


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////// GENERAL ///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




router.route("/")
    .get(
        verification.verifyToken,
        (req, res) => {
            console.log(req.userid)
            profile.getUserByUserid(req.userid)
                .then((result) => {
                    console.log(result)
                    res.status(200).json(result)
                }, (error) => {
                    console.log(result)
                    if (error.message != null) { res.status(404).json(errorHandling.errorMessage(404)) } else { res.status(500).json(result) }
                }, )
        }
    )

.put(
    verification.verifyToken,
    urlencodedParser,
    // bodyParser,
    async(req, res, next) => {
        // console.log(req.body)
        putUsernameByUsernameKeys = {
            username: req.body.username,
            userid: req.userid,
        }
        putUsernameByUsernameResult = await profile.putUsernameByUsername(putUsernameByUsernameKeys)
            .then((result) => {
                return result
            }, (error) => {
                return ({ status: 500, message: errorHandling.errorMessage(500) })
            })
        if (putUsernameByUsernameResult.status === 500) { return res.status(errorHandling.sqlErrorStatus(putUsernameByUsernameResult.error.errno)).json(errorHandling.sqlErrorMessages(putUsernameByUsernameResult.error.errno)) } else if (putUsernameByUsernameResult.status === 409) { return res.status(putUsernameByUsernameResult.status).json(putUsernameByUsernameResult) }

        keys = {
            userid: req.userid,
            email: req.body.email,
            contact: req.body.contact,
        }
        profile.putUserByUserid(keys)
            .then((result) => {
                console.log(result)
                console.log(`messages from updating stuff and stuff`)
                res.status(200).json(result)
            }, (error) => {
                res.status(500).json(errorHandling.errorMessage(500))
            })

        .catch((err) => {
            console.log(errorHandling.errorMessage(500))
            res.status(500).json(errorHandling.errorMessage(500))
        })
    }
)

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////// MISCELLANEOUS ////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

router.route("/getAll")
    .get(
        verification.verifyAdminToken,
        (req, res) => {
            console.log(req.userid)
            profile.getAllUsers()
                .then((result) => {
                    res.status(200).json(result)
                }, (error) => {
                    res.status(errorHandling.sqlErrorStatus(error.errno)).json(errorHandling.sqlErrorMessages(error.errno))
                }, )

            .catch((err) => {
                console.log(errorHandling.errorMessage(500))
                res.status(500).json(errorHandling.errorMessage(500))
            })
        }
    )


router.route("/search")
    .get(
        verification.verifyAdminToken,
        urlencodedParser,
        (req, res) => {
            reqQuery = {
                    name: req.query.name,
                    role: (req.query.role) ? (req.query.role) : '',
                }
                // console.log('request')
                // console.log(reqQuery)
                // filterInfo = cleaningUndefined(reqQuery)
            profile.searchProfile(reqQuery)
                .then((result) => {
                    res.status(200).json(result)
                }, (error) => {
                    if (error.message === "No Users") { res.status(404).json(error) } else { res.status(errorHandling.sqlErrorStatus(error.errno)).json(errorHandling.sqlErrorMessages(error.errno)) }

                })
                .catch(() => { res.status(errorHandling.genericError).json(errorHandling.errorMessage(errorHandling.genericError)) })
        }
    )




const bcrypt = require("bcrypt")
router.route("/changePassword")
    .put(
        urlencodedParser,
        verification.verifyToken,
        async(req, res) => {
            console.log(req.body.newpassword)
            matchPassword = {
                userid: req.userid,
                password: req.body.oldpassword,
            }

            passwordMatch = await profile.loginUserById(matchPassword)
                .then(() => { return ({ success: true }) }, (error) => { return error })
            if (passwordMatch.success !== true) {
                return res.status(404).json(passwordMatch)
            }
            newInfo = {
                userid: req.userid,
                password: await bcrypt.hash(req.body.newpassword, 10),
            }
            profile.putPasswordByUserid(newInfo)
                .then(() => { return res.status(204).json() }, (error) => { return res.status(errorHandling.sqlErrorStatus(error.errno)).json(errorHandling.sqlErrorMessages(error.errno)) })
                .catch(() => {
                    console.log(errorHandling.errorMessage(500))
                    res.status(500).json(errorHandling.errorMessage(500))
                })
        }
    )


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////// IMAGES ///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


const multer = require("multer")
const path = require("path")
const userUploadFolder = path.join(__dirname, "../uploads/userUploads")
const fileStorageEngine = multer.diskStorage({
    destination: (req, file, callback) => {
        callback(null, userUploadFolder)
    },
    filename: (req, file, callback) => {
        callback(null, `${Date.now()}--${file.originalname}`)
    },
})
const upload = multer({ storage: fileStorageEngine })


const fs = require('fs')
const { promisify } = require('util')

const unlinkAsync = promisify(fs.unlink)


app.use(upload.array());
app.use(express.static('public'));


router.route("/profileImage")
    .put(
        verification.verifyToken,
        upload.single("image"),
        async(req, res) => {
            await profile.getUserImageByUserid(req.userid)
                .then(async(filename) => {
                    await unlinkAsync(path.join(userUploadFolder, filename))
                }, () => {
                    return
                })
            console.log(req.file)
            keys = {
                userid: req.userid,
                image_reference: req.file.filename,
            }
            profile.putUserImage(keys)
                .then((result) => {
                    res.status(200).json(result)
                }, (error) => {
                    res.status(errorHandling.sqlErrorStatus(error.errno)).json(errorHandling.sqlErrorMessages(error.errno))
                })
                .catch(() => { res.status(500).json(errorHandling.errorMessage(500)) })

        }
    )





/////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////// INTEREST /////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////

const interest = require("../model/interest");
const { nextTick } = require("process");


router.route("/:userid/interest")
    .get(
        verification.verifyToken,
        (req, res) => {
            getInterestDetails = {
                userid: req.userid,
            }
            interest.getInterest(getInterestDetails)
                .then((result) => {
                    res.status(200).json(result)
                }, (error) => {
                    res.status(errorHandling.sqlErrorStatus(error.errno))
                    console.log(errorHandling.sqlErrorMessages(error.errno))
                    res.json(errorHandling.sqlErrorMessages(error.errno));
                })
                .catch((err) => {
                    console.log(errorHandling.errorMessage(500))
                    res.status(500)
                    res.json(errorHandling.errorMessage(500))
                })
        }
    )
    .post(
        verification.verifyToken,
        urlencodedParser,
        async(req, res) => {
            console.log(req.body)
            await interest.deleteInterest(req.params)
                .then(() => { return }, (error) => {
                    console.log(error)
                    return
                })
                .catch((error) => {
                    console.log(error);
                    return
                })


            errorMap = [];
            categoryids = req.body.categoryids.split(',')

            for (var i = 0; i < categoryids.length; i++) {
                postInterestKeys = {
                    userid: req.params.userid,
                    categoryid: categoryids[i]
                }

                insertStatus = await interest.postInterest(postInterestKeys)
                    .then(() => {
                        // res.status(200).json(result)
                        return (errorHandling.errorMessage(201))
                    }, (error) => {
                        // console.log(error)
                        if (error.code === 'ER_DUP_ENTRY' || error.errno === 1062) {
                            // console.log(errorHandling.errorMessage(409))
                            return (errorHandling.errorMessage(409))
                        } else {
                            console.log(error)
                            return errorHandling.sqlErrorMessages(error.errno)
                        }
                    })
                    .catch((err) => {
                        return (errorHandling.errorMessage(500))
                    })
                console.log(insertStatus)

                errorMap.push(insertStatus)
            }

            res.status(200).json(errorMap)

        }
    )

module.exports = router;