const express = require("express");
const app = express();
const router = express.Router()
const verification = require('../auth/verification.js');
const product = require("../model/product")
const errorHandling = require("../errorHandling/errorMessages")
const bodyParser = require('body-parser');
const urlencodedParser = bodyParser.urlencoded({ extended: false });
const cleaningUndefined = require("../errorHandling/cleaningUndefined")
app.use(bodyParser.json()); //parse appilcation/json data
app.use(urlencodedParser);





////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////// DISCOUNT ////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


const discount = require("../model/discount")

router.route("/")
    .get(
        (req, res) => {
            discount.getAllDiscounts()
                .then((result) => {
                    // console.log(result)
                    res.status(200).json(result)
                }, (error) => { res.status(errorHandling.sqlErrorStatus(error.errno)).json(errorHandling.sqlErrorMessages(error.errno)); })
                .catch(() => { res.status(500).json(errorHandling.errorMessage(500)); })
        }
    )
    .delete(
        verification.verifyAdminToken,
        urlencodedParser,
        (req, res) => {
            discount.deleteDiscountByDiscountId(req.body)
                .then(() => {
                    res.status(204).json(errorHandling.errorMessage(204))
                }, (error) => { res.status(errorHandling.sqlErrorStatus(error.errno)).json(errorHandling.sqlErrorMessages(error.errno)); })
                .catch(() => { res.status(500).json(errorHandling.errorMessage(500)); })
        }
    )

router.route("/:productid")
    .get(
        (req, res) => {
            discount.getDiscountsByProductId(req.params)
                .then((result) => {
                    res.status(200).json(result)
                }, (error) => { res.status(errorHandling.sqlErrorStatus(error.errno)).json(errorHandling.sqlErrorMessages(error.errno)); })
                .catch(() => { res.status(500).json(errorHandling.errorMessage(500)); })
        }
    )
    .post(
        verification.verifyAdminToken,
        urlencodedParser,
        (req, res) => {
            postDiscountDetails = {
                discountname: req.body.discountname,
                productid: req.params.productid,
                discount: req.body.discount,
                start_date: req.body.start_date,
                end_date: req.body.end_date,
            }
            console.log(postDiscountDetails);
            // console.log(postDiscountDetails)
            discount.postDiscount(postDiscountDetails)
                .then(() => {
                    res.status(201).json(errorHandling.errorMessage(201))
                }, (error) => { res.status(errorHandling.sqlErrorStatus(error.errno)).json(errorHandling.sqlErrorMessages(error.errno)); })
                .catch(() => { res.status(500).json(errorHandling.errorMessage(500)); })
        }
    )




router.route("/:productid/filtered")
    .get(
        (req, res) => {
            discount.getFilteredDiscountsByProductId(req.params)
                .then((result) => {
                    res.status(200).json(result)
                }, (error) => { res.status(errorHandling.sqlErrorStatus(error.errno)).json(errorHandling.sqlErrorMessages(error.errno)); })
                .catch(() => { res.status(500).json(errorHandling.errorMessage(500)); })
        }
    )
module.exports = router;