const express = require("express");
const app = express();
const router = express.Router()
const multer = require("multer")
const path = require("path")
const verification = require('../auth/verification.js');
const product = require("../model/product")
const errorHandling = require("../errorHandling/errorMessages")
const bodyParser = require('body-parser');
const urlencodedParser = bodyParser.urlencoded({ extended: false });
const cleaningUndefined = require("../errorHandling/cleaningUndefined")
const discount = require("../model/discount")
const productImage = require("../model/productImage")
const review = require("../model/review")
const productUploadsFolder = path.join(__dirname, "../uploads/productUploads")
app.use(bodyParser.json()); //parse appilcation/json data
app.use(urlencodedParser);








////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////// IMAGES //////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


const fileStorageEngine = multer.diskStorage({
    destination: (req, file, callback) => {
        callback(null, path.join(__dirname, "../uploads/productUploads"))
    },
    filename: (req, file, callback) => {
        callback(null, `${Date.now()}--${file.originalname}`)
    },
})
const upload = multer({ storage: fileStorageEngine })


const fs = require('fs')
const { promisify } = require('util')

const unlinkAsync = promisify(fs.unlink)


router.route("/:productid/productImage")
    .get(
        (req, res) => {
            productImage.getProductImageByProductId(req.params)
                .then((result) => {
                    if (result.length === 0) {
                        res.status(404).json(errorHandling.errorMessage(404))
                    } else {
                        res.status(200).json(result)
                    }
                }, (error) => {
                    res.status(errorHandling.sqlErrorStatus(error.errno)).json(errorHandling.sqlErrorMessages(error.errno))
                }, )
                .catch(() => {
                    res.status(errorHandling.genericError).json(errorHandling.errorMessage(errorHandling.genericError))
                })
        })

.post(
    verification.verifyAdminToken,
    upload.single("image"),
    (req, res) => {
        postImageConfig = {
            productid: req.params.productid,
            filename: req.file.filename,
        }
        productImage.postProductImageByProductId(postImageConfig)
            .then(() => {
                res.status(201)
                res.json({ productid: req.params.productid, message: "Created" })
            }, (error) => {
                res.status(errorHandling.sqlErrorStatus(error.errno))
                res.json(errorHandling.sqlErrorMessages(error.errno))
            }, )
            .catch(() => {
                res.status(errorHandling.genericError)
                res.json(errorHandling.errorMessage(errorHandling.genericError))
            })
    })







////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////// MISCELLANEOUS ///////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



router.route("/getHighest")
    .get(
        (req, res) => {
            product.highestPrice()
                .then((result) => {
                    res.status(200).json(result)
                }, (error) => {
                    res.status(errorHandling.sqlErrorStatus(error.errno)).json(errorHandling.sqlErrorMessages(error.errno))
                })
                .catch(() => { res.status(errorHandling.genericError).json(errorHandling.errorMessage(errorHandling.genericError)) })
        }
    )


router.route("/search")
    .get(
        urlencodedParser,
        (req, res) => {
            reqQuery = {
                    name: req.query.name,
                    brand: req.query.brand,
                    lowerPrice: req.query.lowerPrice,
                    higherPrice: req.query.higherPrice,
                    category: req.query.category ? req.query.category.split(",") : undefined
                }
                // filterInfo = cleaningUndefined(reqQuery)
                // console.log(filterInfo)
            product.searchProduct(reqQuery)
                .then((result) => {
                    res.status(200).json(result)
                }, (error) => {
                    if (error.message === "No Products") { res.status(404).json(error) } else { res.status(errorHandling.sqlErrorStatus(error.errno)).json(errorHandling.sqlErrorMessages(error.errno)) }

                })
                .catch(() => { res.status(errorHandling.genericError).json(errorHandling.errorMessage(errorHandling.genericError)) })
        }
    )










////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////// GENERAL ENDPOINTS ///////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////





router.route("/")
    .get(
        // verification.verifyNoToken,
        (req, res) => {
            product.getProducts()
                .then((sendToClient) => {
                    res.status(200)
                    res.json(sendToClient)
                }, (error) => {
                    console.log(`sql error: ${error}`)
                    res.status(errorHandling.sqlErrorStatus(error.errno)).json(errorHandling.sqlErrorMessages(error.errno));
                })
                .catch((error) => {
                    console.log(error)
                    res.status(500)
                    res.json(errorHandling.errorMessage(500))
                        // res.render("error", errorHandling.errorMessage(500))
                })
                // res.status(200)
                // res.json(products)
        })



router.route("/post")
    .get(
        verification.verifyAdminToken,
        (req, res) => {
            res.status(200).json({ "message": "OK" })
        }
    )
    .post(
        verification.verifyAdminToken,
        urlencodedParser,
        async(req, res) => {
            console.log(req.body)
            var postProductResult = await product.postProduct(req.body)
                .then((productid) => {
                    return ({ success: true, productid: productid })
                }, (error) => {
                    return (errorHandling.sqlErrorMessages(error.errno))
                })
                .catch((err) => {
                    return (errorHandling.errorMessage(500))
                })
            if (postProductResult.success != true) { return res.status(500).json(postProductResult) }
            product.getProductById(postProductResult)
                .then((sendToClient) => {
                    res.status(200).json(sendToClient)
                }, (error) => {
                    console.log(`sql error: ${error}`)
                    res.status(errorHandling.sqlErrorStatus(error)).json(errorHandling.sqlErrorMessages(error));
                })
                .catch((error) => {
                    console.log(error)
                    res.status(500).json(errorHandling.errorMessage(500))
                        // res.render("error", errorHandling.errorMessage(500))
                })
        }
    )




router.route("/:productid")
    .get(
        (req, res, next) => {
            product.getProductById(req.params)
                .then((result) => {
                    console.log("ROUTER ROUTE '/:productid' SUCCESS")
                    if (result == null) {
                        res.status(404)
                        res.json(errorHandling.errorMessage(404))
                    } else {
                        res.status(200)
                        res.json(result)
                    }
                }, (error) => {
                    console.log("SQL ERROR")
                    res.status(errorHandling.sqlErrorStatus(error.errno))
                    res.json(errorHandling.sqlErrorMessages(error.errno))
                }, )
                .catch(() => {
                    console.log("CONTROLLER IN ERROR")
                    res.status(errorHandling.genericError)
                    res.json(errorHandling.errorMessage(errorHandling.genericError))
                })
        })
    .delete(
        async(req, res) => {
            var deleteProductTrueOrFalse = await product.deleteProduct(req.params)
                .then(() => {
                    // res.status(204).send()
                    return ({ success: true, status: 204, message: "Successfully Deleted Product By Product Id" })
                }, (error) => {
                    console.log("SQL ERROR WHEN DELETE IN deleteProductTrueOrFalse")
                    return ({ success: false, status: 500, message: "SQL Error when deleting product by productid" })
                })
                .catch(() => {
                    return ({ success: false, status: 500, message: "Unknown Error when deleting product by productid" })
                })
            var deleteDiscountTrueOrFalse = await discount.deleteDiscountByProductId(req.params)
                .then(() => {
                    return ({ success: true, status: 204, message: "Successfully Deleted Discount By Product Id" })
                }, (error) => {
                    console.log("SQL ERROR WHEN DELETE IN deleteDiscountTrueOrFalse")
                    return ({ success: false, status: 500, message: "SQL Error when deleting discount by productid" })
                })
                .catch(() => {
                    return ({ success: false, status: 500, message: "Unknown Error when deleting discount by productid" })
                })



            await productImage.getProductImageByProductId(req.params)
                .then((filename_array) => {
                    filename_array.map(async(filename_details) => {
                        await unlinkAsync(path.join(productUploadsFolder, filename_details.image_reference))
                    })
                    return
                }, () => {
                    console.log("Error in deleting product -> Error in Unlinking ProductImage -> productImage.getProductImageByProductId ")
                    return
                })

            var deleteProductImageTrueOrFalse = await productImage.deleteProductImageByProductId(req.params)
                .then(() => {
                    // res.status(204).send()
                    return ({ success: true, status: 204, message: "Successfully Deleted ProductImage By Product Id" })
                }, (error) => {
                    console.log("SQL ERROR WHEN DELETE IN deleteProductImageTrueOrFalse")
                    return ({ success: false, status: 500, message: "SQL Error when deleting productImage by productid" })
                })
                .catch(() => {
                    return ({ success: false, status: 500, message: "Unknown Error when deleting productImage by productid" })
                })
            var deleteReviewTrueOrFalse = await review.deleteReviewByProductId(req.params)
                .then(() => {
                    // res.status(204).send()
                    return ({ success: true, status: 204, message: "Successfully Deleted Review By Product Id" })
                }, (error) => {
                    console.log("SQL ERROR WHEN DELETE IN deleteReviewTrueOrFalse")
                    return ({ success: false, status: 500, message: "SQL Error when deleting review by productid" })
                })
                .catch(() => {
                    return ({ success: false, status: 500, message: "Unknown Error when deleting review by productid" })
                })

            if (deleteProductTrueOrFalse && deleteDiscountTrueOrFalse && deleteProductImageTrueOrFalse && deleteReviewTrueOrFalse) {
                res.status(204).json({ message: "Successfully Deleted Product" })
            } else {
                res.status(500).json({
                    message: {
                        deleteProductTrueOrFalse,
                        deleteDiscountTrueOrFalse,
                        deleteProductImageTrueOrFalse,
                        deleteReviewTrueOrFalse
                    }
                })
            }
        }
    )











module.exports = router;