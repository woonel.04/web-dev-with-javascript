const express = require("express");
const app = express();
const router = express.Router()
const verification = require('../auth/verification.js');
const profile = require("../model/profile")
const errorHandling = require("../errorHandling/errorMessages")
const bodyParser = require('body-parser');
const axios = require("axios")
const jwt = require("jsonwebtoken")
const window = require("window")
require("dotenv").config('../.env')
const bcrypt = require("bcrypt")
var urlencodedParser = bodyParser.urlencoded({ extended: false });

app.use(bodyParser.json()); //parse appilcation/json data
app.use(urlencodedParser);


router.route("/")
    .post(
        urlencodedParser,
        verification.verifyNoToken,
        async(req, res) => {
            newUser = {
                username: req.body.username,
                email: req.body.email,
                contact: req.body.contact,
                password: await bcrypt.hash(req.body.password, 10),
                role: 'user',
            }
            profile.postUser(newUser)
                .then((result) => {
                    console.log("Successful /api/register!")
                    res.status(201)
                    res.json(result)
                        // res.redirect("/login")
                }, (error) => {
                    if (error.errno === 1062 || error.code === 'ER_DUP_ENTRY') {
                        res.status(409).json({ message: "Username already exists" })
                    } else {
                        res.status(errorHandling.sqlErrorStatus(error.errno))
                        res.json(errorHandling.sqlErrorMessages(error.errno))
                    }
                })
                .catch((error) => {
                    console.log("Unknown Server Error")
                    res.status(500)
                    res.send(errorHandling.errorMessage(500))
                        // res.redirect("/register")

                })
        })
module.exports = router;