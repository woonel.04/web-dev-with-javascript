const express = require("express");
const app = express();
const router = express.Router()
const verification = require('../auth/verification.js');
const profile = require("../model/profile")
const errorHandling = require("../errorHandling/errorMessages")
const bodyParser = require('body-parser');
const axios = require("axios")
const jwt = require("jsonwebtoken")
const crypto = require("crypto")
    // const saveToken = require("../auth/saveToken")
const path = require("path")
    // console.log(path.join(__dirname, '../../.env'))
require("dotenv").config({ path: path.join(__dirname, '../../.env') })
const passport = require("passport")
const forgotPassword = require("../auth/forgotPassword")
    // const initialisePassport = require("../configs/passport-config")
const urlencodedParser = bodyParser.urlencoded({ extended: false });

const methodOverride = require('method-override');

app.use(bodyParser.json()); //parse appilcation/json data
app.use(urlencodedParser);

const bcrypt = require("bcrypt")


app.use(passport.initialize())
app.use(passport.session())
app.use(methodOverride('_method'))

// initialisePassport(
//     passport,
// )

router.route("/")
    .get(
        verification.verifyNoToken,
        (req, res, ) => {
            res.status(200).send()
                // next()        
        })
    .post(
        verification.verifyNoToken,
        urlencodedParser,
        (req, res) => {
            newUser = {
                    username: req.body.username,
                    role: "Customer",
                    password: req.body.password,
                }
                // console.log(newUser)

            profile.loginUser(newUser)
                .then((result) => {
                    // console.log(result)
                    token = jwt.sign({
                            id: result.id,
                            role: result.role,
                        },
                        process.env.ACCESS_TOKEN_SECRET, {
                            algorithm: "HS256",
                            expiresIn: 60 /* seconds */ * 60 /* minutes*/ * 24 /*hours*/
                        },
                    )

                    var userInfo = {
                        id: result.id,
                        username: result.username,
                        email: result.email,
                        contact: result.contact,
                        role: result.role,
                        profile_pic: result.profile_pic
                    }

                    res.status(200)
                    res.json({ success: true, token: `Bearer ${token}`, userInfo: userInfo })
                }, (error) => {
                    if (error.message === "Password is Incorrect" || error.message === "Username Doesn't Exist") {
                        res.status(404)
                        res.json(error)
                    } else {
                        console.log(`sql error: ${error}`)
                        res.status(errorHandling.sqlErrorStatus(error))
                        res.json(errorHandling.sqlErrorMessages(error));
                    }
                })
                .catch((error) => {
                    console.log(error)
                    res.status(500)
                    res.json(errorHandling.errorMessage(500))
                })
        })


router.route("/forgot")
    .put(
        urlencodedParser,
        async(req, res) => {
            user_info = await profile.getUserByUsername(req.body.username)
                .then((result) => {
                    return result
                }, (error) => {
                    console.log(error)
                    return error
                })
            if (user_info.message != null) { return res.status(404).json(user_info) }
            newPassword = crypto.randomBytes(10).toString('hex')
                // console.log(newInfo.password)
            mailOptions = {
                email: user_info.email,
                username: user_info.username,
                password: newPassword,
            }
            newInfo = {
                userid: user_info.id,
                password: await bcrypt.hash(newPassword, 10),
            }
            forgotPassword(mailOptions)
                .then((result) => {
                    console.log(`function: forgotPassword SUCCESS - ${result}`)
                }, (error) => {
                    console.log(`function: forgotPassword ERROR - ${error}`)
                })
            profile.putPasswordByUserid(newInfo)
                .then(() => {
                    var sendToClientEmail = user_info.email

                    String.prototype.replaceAt = function(index, replacement) {
                        return this.substr(0, index) + replacement + this.substr(index + replacement.length);
                    }
                    for (var i = 0; i < sendToClientEmail.length; i++) {
                        if (user_info.email[i] === '@') { break; } else if (i > 2) { sendToClientEmail = sendToClientEmail.replaceAt(i, "*") }
                    }
                    res.status(200).json({
                        email: sendToClientEmail,
                    })
                }, (error) => {
                    console.log(error)
                    res.status(errorHandling.genericError).json(errorHandling.errorMessage(errorHandling.genericError))
                })

        }
    )
module.exports = router;