const dbConn = require('./dbConn')


module.exports = {

    postInterest: (keys) => {
        return new Promise((resolve, reject) => {

            dbConn.getConnection()
                .then((connection) => {
                    conn = connection;
                    return conn.query(`INSERT INTO interest (userid, category_id) value (?,?)`, [keys.userid, keys.categoryid])
                })
                .then((result) => {
                    // console.log(result)
                    conn.end()
                    return resolve(result)
                }, (error) => {
                    // console.log("am i in error?")
                    return reject(error)
                })
                .catch((error) => {
                    err = {
                        code: error.code,
                        errno: error.errno,
                    }
                    console.log(err)
                    conn.end()

                    return reject(err)
                })

        })
    },



    getInterest: (keys) => {
        return new Promise((resolve, reject) => {

            dbConn.getConnection()
                .then((connection) => {
                    conn = connection;
                    return conn.query(`
                    select  * from interest where userid = ?`, [keys.userid])
                })
                .then((result) => {
                    // console.log(result)
                    conn.end()
                    return resolve(result[0])
                }, (error) => {
                    // console.log("am i in error?")
                    return reject(error)
                })
                .catch((error) => {
                    err = {
                        code: error.code,
                        errno: error.errno,
                    }
                    console.log(err)
                    conn.end()

                    return reject(err)
                })

        })
    },


    deleteInterest: (keys) => {
        return new Promise((resolve, reject) => {

            dbConn.getConnection()
                .then((connection) => {
                    conn = connection;
                    return conn.query(`delete from interest where userid = ?`, [keys.userid])
                })
                .then((result) => {
                    // console.log(result)
                    conn.end()
                    return resolve(result[0])
                }, (error) => {
                    // console.log("am i in error?")
                    return reject(error)
                })
                .catch((error) => {
                    err = {
                        code: error.code,
                        errno: error.errno,
                    }
                    console.log(err)
                    conn.end()

                    return reject(err)
                })

        })
    },


}