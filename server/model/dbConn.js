var mysql = require('mysql2/promise');


module.exports = {

    getConnection: function() {
        var conn = mysql.createConnection({
                host: "localhost",
                user: "root",
                password: MYSQL_PASSWORD,
                database: MYSQL_DATABASE,
            }

        );

        return conn;

    }
}
