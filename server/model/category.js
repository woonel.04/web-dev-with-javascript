const dbConn = require("./dbConn")
var conn;

module.exports = {
    getCategories: () => {
        return new Promise((resolve, reject) => {
            dbConn.getConnection()
                .then((connection) => {
                    conn = connection;
                    console.log("Connected!")
                    return conn.query(`SELECT * FROM category`)
                })
                .then(result => {
                    console.log("QUERY SUCCESS!")
                    if (result[0].length == 0) {
                        conn.end()
                        return resolve("No Categories Yet")
                    } else {
                        conn.end()
                        return resolve(result[0])
                    }
                })
                .catch((error) => {

                    err = {
                        code: error.code,
                        errno: error.errno,
                    }
                    console.log(err)
                    conn.end()
                    return reject(err)
                })
        })
    },
    postCategory: (keys) => {
        return new Promise((resolve, reject) => {
            dbConn.getConnection()
                .then((connection) => {
                    conn = connection;
                    return conn.query(`INSERT INTO category (category, description) value (?, ?)`, [keys.category, keys.description])
                })
                .then((result) => {
                    conn.end()
                    return resolve(result)
                })
                .catch((error) => {
                    console.log(error)
                    err = {
                        code: error.code,
                        errno: error.errno,
                    }
                    console.log(err)
                    conn.end()
                    return reject(err)

                })
        })
    },
    getProductsByCategoryid: (categoryid) => {
        return new Promise((resolve, reject) => {
            dbConn.getConnection()
                .then((connection) => {
                    conn = connection;
                    console.log("getProductsByCategoryid- Connected!")
                    return conn.query(`SELECT * FROM product where categoryid = ?`, [categoryid])
                })
                .then(result => {
                    console.log("getProductsByCategoryid - QUERY SUCCESS!")
                    conn.end()
                    return resolve(result[0])
                })
                .catch((error) => {

                    err = {
                        code: error.code,
                        errno: error.errno,
                    }
                    console.log(err)
                    conn.end()
                    return reject(err)
                })
        })
    },

}