const dbConn = require("./dbConn")
var conn;
module.exports = {
    getReview: (keys) => {
        return new Promise((resolve, reject) => {
            dbConn.getConnection()
                .then((connection) => {
                    conn = connection;
                    return conn.query(`SELECT r.productid, r.userid, u.username, u.profile_pic, r.rating, r.review , r.created_at 
                    FROM review as r 
                    LEFT JOIN users as u on r.userid = u.id where r.productid =?
                    `, [keys.productid])
                })
                .then((result) => {
                    conn.end();
                    console.log(result[0])
                    return resolve(result[0])
                })
                .catch((error) => {
                    err = {
                        code: error.code,
                        errno: error.errno,
                    }
                    console.log(err)
                    conn.end()
                    return reject(err)
                })
        })
    },
    postReview: (keys) => {
        return new Promise((resolve, reject) => {
            dbConn.getConnection()
                .then((connection) => {
                    conn = connection;
                    return conn.query(`INSERT into review (productid, userid, rating, review) value (?, ?, ?, ?)`, [keys.productid, keys.userid, keys.rating, keys.review])
                })
                .then(() => {
                    console.log(`postReview - INSERT QUERY SUCCESS`)
                    return conn.query(`SELECT LAST_INSERT_ID();`)
                })
                .then((result) => {
                    conn.end();
                    return resolve(result[0][0]["LAST_INSERT_ID()"])
                })
                .catch((error) => {
                    err = {
                        code: error.code,
                        errno: error.errno,
                    }
                    console.log(err)
                    conn.end()
                    return reject(err)
                })
        })
    },




    deleteReviewByProductId: (keys) => {
        return new Promise((resolve, reject) => {

            dbConn.getConnection()
                .then((connection) => {
                    conn = connection;
                    return conn.query(`DELETE FROM review WHERE  productid =?`, [keys.productid])
                })
                .then((result) => {
                    // console.log(result)
                    conn.end()
                    return resolve(result)
                }, (error) => {
                    // console.log("am i in error?")
                    return reject(error)
                })
                .catch((error) => {
                    err = {
                        code: error.code,
                        errno: error.errno,
                    }
                    console.log(err)
                    conn.end()

                    return reject(err)
                })

        })
    },
}