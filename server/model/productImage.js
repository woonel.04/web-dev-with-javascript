const dbConn = require('./dbConn')
var conn

module.exports = {
    getProductImageByProductId: keys => {
        return new Promise((resolve, reject) => {
            dbConn.getConnection()
                .then((connection) => {
                    conn = connection;
                    console.log("Connected")
                    return conn.query(`SELECT * FROM productimage WHERE productid=?`, [keys.productid])
                })
                .then((result) => {
                    console.log(`QUERY SUCCESS`)
                    conn.end()
                    return resolve(result[0])
                })
                .catch((error) => {
                    console.log(`ERROR IN MODEL`)
                    err = {
                        code: error.code,
                        errno: error.errno,
                    }
                    console.log(err)
                    conn.end()
                    return reject(err)
                })

        })
    },
    postProductImageByProductId: (keys) => {
        return new Promise((resolve, reject) => {
            dbConn.getConnection()
                .then((connection) => {
                    conn = connection;
                    console.log("postProductImageByProductId - DbConn")

                    return conn.query(`INSERT INTO productimage (productid, image_reference) value (?,?)`, [keys.productid, keys.filename])
                })
                .then((result) => {
                    console.log(`postProductImageByProductId - INSERT QUERY SUCCESS`)
                    conn.end()
                    return resolve(result)
                })
                .catch((error) => {
                    console.log(error)
                    err = {
                        code: error.code,
                        errno: error.errno,
                    }
                    console.log(err)
                    conn.end()
                    return reject(err)
                })

        })
    },




    deleteProductImageByProductId: (keys) => {
        return new Promise((resolve, reject) => {

            dbConn.getConnection()
                .then((connection) => {
                    conn = connection;
                    return conn.query(`DELETE FROM productimage WHERE  productid =?`, [keys.productid])
                })
                .then((result) => {
                    conn.end()
                    return resolve(result)
                }, (error) => {

                    return reject(error)
                })
                .catch((error) => {
                    err = {
                        code: error.code,
                        errno: error.errno,
                    }
                    console.log(err)
                    conn.end()

                    return reject(err)
                })

        })
    },
}