const dbConn = require('./dbConn')
var conn;

module.exports = {
    getAllDiscounts: () => {
        return new Promise((resolve, reject) => {
            dbConn.getConnection()
                .then((connection) => {
                    conn = connection;
                    return conn.query(`
                    select d.discountid, d.discountname, d.productid, p.name, d.discount, p.price, (p.price * ((100 - d.discount) / 100)) as discounted_price,  cast(d.start_date as date) as start_date, cast(d.end_date as date) as end_date, i.image_reference
                    from discount as d
                    left join product as p on d.productid = p.productid 
                    left join productimage as i on d.productid = i.productid
                    where start_date < now() and now()< end_date 
                    group by productid
                    `)
                })
                .then((result) => {
                    conn.end()
                    return resolve(result[0])
                })
                .catch((error) => {
                    err = {
                        code: error.code,
                        errno: error.errno,
                    }
                    console.log(err)
                    conn.end()

                    return reject(err)
                })
        })
    },

    postDiscount: (keys) => {
        return new Promise((resolve, reject) => {

            dbConn.getConnection()
                .then((connection) => {
                    conn = connection;
                    return conn.query(`INSERT INTO discount (discountname, productid, discount, start_date, end_date) VALUES (?, ?,?,?,?)`, [keys.discountname, keys.productid, keys.discount, keys.start_date, keys.end_date])
                })
                .then((result) => {
                    // console.log(result)
                    conn.end()
                    return resolve(result)
                }, (error) => {
                    // console.log("am i in error?")
                    return reject(error)
                })
                .catch((error) => {
                    err = {
                        code: error.code,
                        errno: error.errno,
                    }
                    console.log(err)
                    conn.end()

                    return reject(err)
                })

        })
    },

    getDiscountsByProductId: (keys) => {
        return new Promise((resolve, reject) => {
            dbConn.getConnection()
                .then((connection) => {
                    conn = connection;
                    return conn.query(`
                    select d.discountid, d.discountname, d.productid, p.name , p.price,  d.discount, (p.price * ((100 - d.discount) / 100)) as discounted_price,  cast(d.start_date as date) as start_date, cast(d.end_date as date) as end_date, i.image_reference
                    from discount as d
                    left join product as p on d.productid = p.productid 
                    left join productimage as i on d.productid = i.productid
                    where  d.productid = ?
                    group by d.discountname
                    `, [keys.productid])
                })
                .then((result) => {
                    conn.end()
                    return resolve(result[0])
                })
                .catch((error) => {
                    err = {
                        code: error.code,
                        errno: error.errno,
                    }
                    console.log(err)
                    conn.end()

                    return reject(err)
                })
        })
    },


    getFilteredDiscountsByProductId: (keys) => {
        return new Promise((resolve, reject) => {
            dbConn.getConnection()
                .then((connection) => {
                    conn = connection;
                    return conn.query(`
                    select d.discountid, d.discountname, d.productid, p.name , p.price,  d.discount, (p.price * ((100 - d.discount) / 100)) as discounted_price,  cast(d.start_date as date) as start_date, cast(d.end_date as date) as end_date, i.image_reference
                    from discount as d
                    left join product as p on d.productid = p.productid 
                    left join productimage as i on d.productid = i.productid
                    where start_date < now() and now()< end_date 
                    and  d.productid = ?
                    group by d.discountname
                    `, [keys.productid])
                })
                .then((result) => {
                    conn.end()
                    return resolve(result[0][0])
                })
                .catch((error) => {
                    err = {
                        code: error.code,
                        errno: error.errno,
                    }
                    console.log(err)
                    conn.end()

                    return reject(err)
                })
        })
    },


    deleteDiscountByDiscountId: (keys) => {
        return new Promise((resolve, reject) => {

            dbConn.getConnection()
                .then((connection) => {
                    conn = connection;
                    return conn.query(`DELETE FROM discount WHERE  discountid =?`, [keys.discountid])
                })
                .then((result) => {
                    // console.log(result)
                    conn.end()
                    return resolve(result)
                }, (error) => {
                    // console.log("am i in error?")
                    return reject(error)
                })
                .catch((error) => {
                    err = {
                        code: error.code,
                        errno: error.errno,
                    }
                    console.log(err)
                    conn.end()

                    return reject(err)
                })

        })
    },




    deleteDiscountByProductId: (keys) => {
        return new Promise((resolve, reject) => {

            dbConn.getConnection()
                .then((connection) => {
                    conn = connection;
                    return conn.query(`DELETE FROM discount WHERE  productid =?`, [keys.productid])
                })
                .then((result) => {
                    // console.log(result)
                    conn.end()
                    return resolve(result)
                }, (error) => {
                    // console.log("am i in error?")
                    return reject(error)
                })
                .catch((error) => {
                    err = {
                        code: error.code,
                        errno: error.errno,
                    }
                    console.log(err)
                    conn.end()

                    return reject(err)
                })

        })
    },

    deleteDiscountByExpireDate: () => {
        return new Promise((resolve, reject) => {

            dbConn.getConnection()
                .then((connection) => {
                    conn = connection;
                    return conn.query(`delete from discount where now() > end_date`)
                })
                .then((result) => {
                    // console.log(result)
                    conn.end()
                    return resolve(result)
                }, (error) => {
                    // console.log("am i in error?")
                    return reject(error)
                })
                .catch((error) => {
                    err = {
                        code: error.code,
                        errno: error.errno,
                    }
                    console.log(err)
                    conn.end()

                    return reject(err)
                })

        })

    },
}