const dbConn = require("../model/dbConn")
const bcrypt = require("bcrypt");
var conn;
module.exports = {
    loginUser: keys => {
        return new Promise((resolve, reject) => {
            dbConn.getConnection()
                .then((connection) => {
                    conn = connection;
                    console.log("Connected!")
                    return conn.query('select * from users where username=?', [keys.username])
                })
                .then(async result => {
                    console.log("1 - QUERY SUCCESS!")
                    console.log(`Found ${result[0].length} matching match for ${keys.username}`)
                    if (result[0].length == 0) {
                        conn.end()
                        return reject({ message: "Username Doesn't Exist" })

                    } else if (!(await bcrypt.compare(keys.password, result[0][0].password))) {
                        reject({ message: "Password is Incorrect" })
                    } else {
                        return resolve(result[0][0])
                    }
                })
        })
    },


    loginUserById: keys => {
        return new Promise((resolve, reject) => {
            dbConn.getConnection()
                .then((connection) => {
                    conn = connection;
                    console.log("Connected!")
                    return conn.query('select * from users where id=?', [keys.userid])
                })
                .then(async result => {
                    console.log("1 - QUERY SUCCESS!")
                    console.log(`Found ${result[0].length} matching match for ${keys.userid}`)
                    if (result[0].length == 0) {
                        conn.end()
                        return reject({ message: "User Doesn't Exist" })

                    } else if (!(await bcrypt.compare(keys.password, result[0][0].password))) {
                        reject({ message: "Password is Incorrect" })
                    } else {
                        return resolve(result[0][0])
                    }
                })
        })
    },


    postUser: keys => {
        return new Promise((resolve, reject) => {
            dbConn.getConnection()
                .then((connection) => {
                    conn = connection;
                    console.log(`conn SUCCESS`)
                        // console.log(keys)
                    return conn.query(`INSERT INTO users (username, email, contact, password) value (?, ?, ?, ?)`, [keys.username, keys.email, keys.contact, keys.password])
                })
                .then((result) => {
                    conn.end();

                    return resolve(result)
                })
                .catch((error) => {
                    err = {
                        code: error.code,
                        errno: error.errno,
                    }
                    console.log(err)
                    conn.end()
                    return reject(err)
                });

        })
    },
    getUserByUserid: (userid) => {
        return new Promise((resolve, reject) => {
            dbConn.getConnection()
                .then((connection) => {
                    console.log("getUserByUserid - Connected")
                    conn = connection;
                    return conn.query(`SELECT * FROM users where id = ?`, [userid])
                })
                .then((result) => {
                    console.log("getUserByUserid - QUERY SUCCESS")
                    conn.end()

                    if (result[0].length > 0) { return resolve(result[0][0]) } else { return reject({ message: "Userid Doesn't Exist" }) }
                })
                .catch((error) => {
                    err = {
                        code: error.code,
                        errno: error.errno,
                    }
                    console.log(err)
                    conn.end()
                    return reject(err)
                });
        })
    },






    getUseridByUsername: (username) => {
        return new Promise((resolve, reject) => {
            dbConn.getConnection()
                .then((connection) => {
                    conn = connection;
                    return conn.query(`SELECT id  FROM users where username =? `, [username])
                })
                .then(result => {
                    console.log(`getUseridByUsername - QUERY SUccess!!`)
                    conn.end()
                    if (result[0][0] == null) {
                        return reject({ message: "Username Doesn't Exist" })
                    } else {
                        return resolve(result[0][0].id)
                    }
                })
                .catch(error => {
                    err = {
                        code: error.code,
                        errno: error.errno,
                    }
                    console.log(err)
                    conn.end()
                    return reject(error)
                })
        })
    },
    getUserByUsername: (username) => {
        return new Promise((resolve, reject) => {
            dbConn.getConnection()
                .then((connection) => {
                    conn = connection;
                    return conn.query(`SELECT * FROM users where username = ?`, [username])
                })
                .then(result => {
                    conn.end()
                    if (result[0].length > 0) {
                        console.log(`getUserByUsername - QUERY SUccess!!`)
                        return resolve(result[0][0])
                    } else {
                        return reject({ message: "Username Doesn't Exist" })
                    }
                })
                .catch(error => {
                    err = {
                        code: error.code,
                        errno: error.errno,
                    }
                    console.log(err)
                    conn.end()
                    return reject(err)
                })
        })
    },


    getUserImageByUserid: (userid) => {
        return new Promise((resolve, reject) => {
            dbConn.getConnection()
                .then((connection) => {
                    conn = connection;
                    return conn.query(`SELECT profile_pic FROM users where id = ?`, [userid])
                })
                .then(result => {
                    console.log(`getUserImageByUserid result => ${result[0][0].profile_pic}`)
                    conn.end()
                    if (result[0][0].profile_pic !== "defaultUser.png") {
                        return resolve(result[0][0].profile_pic)
                    } else {
                        return reject()
                    }
                })
                .catch(error => {
                    err = {
                        code: error.code,
                        errno: error.errno,
                    }
                    console.log(err)
                    conn.end()
                    return reject(err)
                })
        })
    },

    getAllUsers: () => {
        return new Promise((resolve, reject) => {
            dbConn.getConnection()
                .then((connection) => {
                    conn = connection;
                    return conn.query(`SELECT id, username, email, contact, role, profile_pic FROM users  `)
                })
                .then(result => {
                    console.log(`getAllUsers - QUERY SUccess!!`)
                    conn.end()
                    return resolve(result[0])
                })
                .catch(error => {
                    err = {
                        code: error.code,
                        errno: error.errno,
                    }
                    console.log(err)
                    conn.end()
                    return reject(error)
                })
        })

    },


    ////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////// SEARCH USER  /////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////


    searchProfile: (keys) => {
        return new Promise((resolve, reject) => {
            dbConn.getConnection()
                .then((connection) => {
                    conn = connection;
                    return conn.query(`SELECT id, username, email, contact, role, profile_pic FROM users WHERE (username LIKE concat('%' , ?,'%') ) and (role like concat('%',?,'%'))`, [keys.name, keys.role])
                })
                .then(result => {
                    console.log(`searchProfile - QUERY SUccess!!`)
                    console.log(result[0])
                    if (result[0].length === 0) return reject({ status: 404, message: "No Users" })
                    conn.end()
                    return resolve(result[0])
                })
                .catch(error => {
                    err = {
                        code: error.code,
                        errno: error.errno,
                    }
                    console.log(err)
                    conn.end()
                    return reject(error)
                })
        })

    },







    ////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////// UPDATE USER DETAILS ///////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////


    putUsernameByUsername: (keys) => {
        return new Promise((resolve, reject) => {
            dbConn.getConnection()
                .then((connection) => {
                    console.log("getUserByUserid - Connected")
                    conn = connection;
                    return conn.query(`SELECT id FROM users where username =? `, [keys.username])
                })
                .then((result) => {
                    console.log(result[0])
                    if (result[0].length > 0) {
                        if (result[0][0].id === keys.userid) {
                            return resolve({ status: 200, message: "Username belongs to Userid" })
                        } else {
                            return resolve({ status: 409, message: "Username Already Exists" })
                        }
                    } else {
                        console.log(result[0].length)
                        return conn.query(`UPDATE users SET username =? where id=?`, [keys.username, keys.userid])
                    }
                })
                .then((result) => {
                    conn.end()
                    console.log("putUsernameByUsername - QUERY SUCCESS")
                    return resolve({ status: 204, message: "Username Updated Successfully" })
                })
                .catch((error) => {
                    err = {
                        code: error.code,
                        errno: error.errno,
                    }
                    console.log(err)
                    conn.end()
                    return reject(err)
                });
        })
    },

    putUserByUserid: (keys) => {
        return new Promise((resolve, reject) => {
            dbConn.getConnection()
                .then((connection) => {
                    console.log("getUserByUserid - Connected")
                    conn = connection;
                    return conn.query(`UPDATE users SET email =? , contact =? where id =? `, [keys.email, keys.contact, keys.userid])
                })
                .then((result) => {
                    console.log("putUserByUserid - QUERY SUCCESS")
                    conn.end()
                    return resolve({ status: 204, message: "User Details Updated Successfully" })
                })
                .catch((error) => {
                    err = {
                        code: error.code,
                        errno: error.errno,
                    }
                    console.log(err)
                    conn.end()
                    return reject(err)
                });
        })
    },

    putUserImage: (keys) => {
        return new Promise((resolve, reject) => {
            dbConn.getConnection()
                .then((connection) => {
                    console.log("putUserImage - Connected")
                    conn = connection;
                    return conn.query(`UPDATE users SET profile_pic =? where id =? `, [keys.image_reference, keys.userid])
                })
                .then((result) => {
                    console.log("putUserImage - QUERY SUCCESS")
                    conn.end()
                    return resolve({ status: 204, message: "User Details Updated Successfully" })

                })
                .catch((error) => {
                    err = {
                        code: error.code,
                        errno: error.errno,
                    }
                    console.log(err)
                    conn.end()
                    return reject(err)
                });
        })
    },
    putPasswordByUserid: (keys) => {
        return new Promise((resolve, reject) => {
            dbConn.getConnection()
                .then((connection) => {
                    console.log("putPasswordByUserid - Connected")
                    conn = connection;
                    return conn.query(`UPDATE users SET password =? where id =? `, [keys.password, keys.userid])
                })
                .then((result) => {
                    console.log("putUserByUserid - QUERY SUCCESS")
                    conn.end()
                    return resolve({ status: 204, message: "User Details Updated Successfully" })
                })
                .catch((error) => {
                    err = {
                        code: error.code,
                        errno: error.errno,
                    }
                    console.log(err)
                    conn.end()
                    return reject(err)
                });
        })

    },





}