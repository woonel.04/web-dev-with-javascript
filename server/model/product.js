const dbConn = require('./dbConn')
var conn;
module.exports = {
    getProducts: () => {
        return new Promise((resolve, reject) => {
            dbConn.getConnection()
                .then((connection) => {
                    conn = connection;
                    console.log("Connected!")
                    return conn.query(
                        `
                    select p.productid, p.name, p.description , c.category, p.brand, p.price , avg(r.rating) as avg_rating ,i.image_reference 
                    from product as p 
                    inner join category as c on p.categoryid = c.categoryid 
                    inner join productimage as i on p.productid = i.productid 
                    left join review as r on p.productid = r.productid
                    group by p.productid;
                    `
                    )
                })
                .then(result => {
                    console.log("QUERY SUCCESS!")
                    if (result[0].length == 0) {
                        conn.end()
                        return resolve("No Products Yet")
                    } else {
                        conn.end()
                        return resolve(result[0])
                    }
                })
                .catch((error) => {

                    err = {
                        code: error.code,
                        errno: error.errno,
                    }
                    console.log(err)
                    conn.end()
                    return reject(err)
                })
        })
    },
    postProduct: keys => {
        return new Promise((resolve, reject) => {
            dbConn.getConnection()
                .then((connection) => {
                    conn = connection;
                    console.log(`conn SUCCESS`)
                    return conn.query(`INSERT INTO product (name, description, categoryid, brand, price) VALUE (?, ?, ?, ?, ?)`, [keys.name, keys.description, keys.categoryid, keys.brand, keys.price])
                })
                .then(() => {
                    console.log(`INSERT QUERY SUCCESS`)
                    return conn.query(`SELECT LAST_INSERT_ID();`)
                })
                .then((result) => {
                    conn.end();
                    return resolve(result[0][0]["LAST_INSERT_ID()"])
                })
                .catch((error) => {
                    err = {
                        code: error.code,
                        errno: error.errno,
                    }
                    console.log(err)
                    conn.end()
                    return reject(err)
                })
        })
    },
    getProductById: keys => {
        return new Promise((resolve, reject) => {
            dbConn.getConnection()
                .then((connection) => {
                    conn = connection;
                    console.log("Connected!")
                    return conn.query(
                        `
                                            
                        select p.productid, p.name, p.description , c.category, p.brand, p.price , i.image_reference , avg(r.rating) as avg_rating
                        from product as p 
                        left join category as c on p.categoryid = c.categoryid 
                        left join productimage as i on p.productid = i.productid 
                        left join review as r on p.productid = r.productid
                        where p.productid = ?
                        group by p.productid
                    `, [keys.productid])
                })
                .then((result) => {
                    console.log("QUERY SUCCESS!")
                    conn.end()
                    return resolve(result[0][0])
                })
                .catch((error) => {
                    err = {
                        code: error.code,
                        errno: error.errno,
                    }
                    console.log(err)
                    conn.end()
                    return reject(err)
                })
        })
    },

    searchProduct: keys => {
        return new Promise((resolve, reject) => {
            dbConn.getConnection()
                .then((connection) => {
                    conn = connection;
                    console.log("Connected!")

                    var QUERY = `
                    SELECT p.productid, p.name, p.description , c.category, p.brand, p.price , i.image_reference 
                    FROM product AS p  
                    INNER JOIN category as c on p.categoryid = c.categoryid 
                    INNER JOIN productimage as i on p.productid = i.productid 
                    where (p.name like concat('%', ?, '%') or p.brand like concat('%', ? , '%'))
                    and (p.price between ? and ?) 
                    `
                    var VARIABLES = [keys.name, keys.name, keys.lowerPrice, keys.higherPrice]

                    if (keys.category != null) {
                        QUERY += `and (`
                        for (var categoryIndex = 0; categoryIndex < keys.category.length; categoryIndex++) {
                            VARIABLES.push(keys.category[categoryIndex])
                            QUERY += `c.category = ? `
                            if (categoryIndex < keys.category.length - 1) { QUERY += ` or ` }
                        }
                        QUERY += `)`
                    }

                    QUERY += ` group by p.productid`
                    console.log(QUERY)
                    console.log(VARIABLES)
                    return conn.query(QUERY, VARIABLES)
                })
                .then((result) => {
                    console.log("QUERY SUCCESS!")
                    conn.end()
                    if (result[0].length === 0) { return reject({ message: "No Products" }) } else { return resolve(result[0]) }
                })
                .catch((error) => {
                    console.log(error)
                    err = {
                        code: error.code,
                        errno: error.errno,
                    }
                    console.log(err)
                    conn.end()
                    return reject(err)
                })
        })
    },
    highestPrice: () => {
        return new Promise((resolve, reject) => {
            dbConn.getConnection()
                .then((connection) => {
                    conn = connection;
                    return conn.query(`select max(price) from product`)
                })
                .then((result) => {
                    console.log(result[0][0])
                    conn.end()
                    return resolve(result[0])
                })
                .catch((error) => {
                    console.log(error)
                    err = {
                        code: error.code,
                        errno: error.errno,
                    }
                    console.log(err)
                    conn.end()
                    return reject(err)
                })
        })
    },


    deleteProduct: (keys) => {
        return new Promise((resolve, reject) => {
            dbConn.getConnection()
                .then((connection) => {
                    conn = connection;
                    return conn.query(`DELETE FROM product WHERE productid=?`, [keys.productid])
                })
                .then((result) => {
                    conn.end()
                    return resolve(result)
                })
                .catch((error) => {
                    console.log(error)
                    err = {
                        code: error.code,
                        errno: error.errno,
                    }
                    console.log(err)
                    conn.end()
                    return reject(err)
                })
        })
    }
}