const express = require("express");
const app = express();
const path = require("path")
const bodyParser = require("body-parser")
const cors = require("cors")
const errorHandling = require("./errorHandling/errorMessages")
//port for client-side
const port = 8000;
//port for server
// const port = 8888

app.use(bodyParser.json());
bodyParser.urlencoded({extended:false})
app.use(cors());


// app.set("views", path.join(__dirname , "../views"))
// app.use(express.static("views"))
// app.set("view engine", "ejs")
// app.use("/static", express.static('../views/static/'));


// app.use("/api/productImage", express.static('./uploads/productUploads'))
// app.use("/api/userImage", express.static('./uploads/userUploads'))
app.use("/uploads", express.static("./uploads"))


//routes
// app.use("/api/users", require("./controller/users"))
// app.use("/api/users", require("./controller/users"))
app.use("/api/login", require("./controller/login"))
app.use("/api/register", require("./controller/register"))
app.use("/api/products", require("./controller/products"))
app.use("/api/profile", require("./controller/profile"))
app.use("/api/category", require("./controller/category"))
app.use("/api/discount", require("./controller/discount"))
// app.use("/api/productImage", require("./controller/productImage"))
app.use("/api/review", require("./controller/review"))
// app.use("/api/interest", require("./controller/interest"))
// app.use("/products", require("./controller/products"))
// app.use("/api", require("./controller/api"))
// app.use("/", require("./controller/index"))



//error handling


app.listen(port, ()=> {
    console.log(`PORT RUNNING ON : ${port}`)
})