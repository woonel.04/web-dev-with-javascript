var jwt=require('jsonwebtoken');
require("dotenv").config("../../.env/.env")
var jwt_decode = require("jwt-decode")
const profile = require("../model/profile")
// var config=require('../configs/config');
const errorHandling = require("../errorHandling/errorMessages");
const { genericError } = require('../errorHandling/errorMessages');

module.exports = {
    verifyToken: (req, res, next) => {
        const authHeader = req.headers["authorization"]
        const token = authHeader && authHeader.split(" ")[1]
        if (token == null) {
            return res.status(401).json(errorHandling.errorMessage(401))

        }
        jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user)=> {
            if (err) {

                return res.status(403).send(errorHandling.errorMessage(403))
            }
            else {
                
                req.userid=jwt_decode(token).id; //decode the userid and store in req for use
                req.role=jwt_decode(token).role; //decode the role and store in req for use
                next();
            }
        })
    },
    verifyAdminToken: (req, res, next) => {
        const authHeader = req.headers["authorization"]
        const token = authHeader && authHeader.split(" ")[1]
        
        if (token == null) {
            return res.status(401).json(errorHandling.errorMessage(401))
        }
        jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user)=> {
            if (err) {

                return res.status(403).send(errorHandling.errorMessage(403))
            }
            else {
                
                req.userid=jwt_decode(token).id; //decode the userid and store in req for use
                req.role=jwt_decode(token).role; //decode the role and store in req for use
                if (req.role == 'Customer') {
                    return res.status(403).json(errorHandling.errorMessage(403))
                }
                isAdmin = true;
                console.log(`verifyAdminToken has verified that user ${req.userid} is ${req.role}` )
                next();

            }
        })
    },
    verifyNoToken: (req, res, next)=> {
        const authHeader = req.headers["authorization"]
        const token = authHeader && authHeader.split(" ")[1]
        if (token != null) {
            return res.status(303).json(errorHandling.errorMessage(303))
        }
        next()
    },

    verifyAdminTokenAndUser: (req, res, next) => {
        const authHeader = req.headers["authorization"]
        const token = authHeader && authHeader.split(" ")[1]
        
        if (token == null) {
            console.log("verifyTokenByUserId has verified that the user does not have a token")
            return res.status(401).json(errorHandling.errorMessage(401))
        }
        jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user)=> {
            if (err) {
                console.log("verifyTokenByUserId has verified that the token has thrown an error")
                return res.status(403).json(errorHandling.errorMessage(403))
            }
            else {
                req.userid=jwt_decode(token).id; //decode the userid and store in req for use
                req.role=jwt_decode(token).role; //decode the role and store in req for use
                // if (req.params.username != null) { username = req.params.username} else if (req.body.username )
                return profile.getUseridByUsername(req.params.username).then((userid)=>{

                    if (req.userid === userid || req.role == "Admin") {
                        return next()
                    } else {
                        return res.status(401).json(errorHandling.errorMessage(401))
                    }
                },(error)=>{
                    res.status(404).json({message:error})                    
                })
                .catch(()=>{
                    console.log(`Error - verification.js, function: profile.getUseridByUsername. error: \n${error}`)
                    return res.status(errorHandling.genericError).json(errorHandling.errorMessage(errorHandling.genericError))
                })
                next();
            }
        })
    },
}
// // module.exports=verifyToken;
