const path = require("path")
require("dotenv").config({path:path.join(__dirname, "../../.env")})
const nodemailer = require("nodemailer")
const { resolve } = require("path/posix")
let transporter = nodemailer.createTransport({
    service: 'gmail' ,
    auth: {
        user: process.env.EMAIL,
        pass: process.env.PASSWORD,
    },
})



module.exports = (keys) => {
    return new Promise((resolve, reject)=> {
        mailOptions = {
            from : 'placeholder.company.1234567890@gmail.com',
            to: keys.email,
            subject: `New Password for ${keys.username}`,
            text: `New Password: ${keys.password}`,
        }
        // console.log(mailOptions)
        return transporter.sendMail(mailOptions, (err, result)=> {
            if (err) {
                console.log(`transport.sendMail error: ${err}`)
                return reject(err)
            }
            else {
                console.log(`transport.sendMail SUCCESS: ${result}`)    
                return resolve(result)        
            }
        })
    })
}
