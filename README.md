# Web Dev with Javascript


This is a Web Development project using Javascript for front end and backend, and MYSQL for the database. 


## Applicability of Code

Admittedly, the code isn't very replicable, due to the MYSQL database. I'm currently working on a building a simple docker image to contain an MYSQL or MSSQL container.
